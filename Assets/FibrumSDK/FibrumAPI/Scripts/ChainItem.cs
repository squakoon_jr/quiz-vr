using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChainItem
{
	public Func<Action, Action, IEnumerator> func;
	public Action onSuccess;
	public Action onFail;

	public ChainItem(Func<Action, Action, IEnumerator> _func,Action _onSuccess, Action _onFail)
	{
		this.func = _func;
		this.onSuccess = _onSuccess;
		this.onFail = _onFail;
	}
}

