﻿using System;

namespace Assets.FibrumAPI.Scripts
{
    public static class FibrumAPIPayments
    {
        [Serializable]
        public class AndroidPurchase
        {
            public string userGuid;
            public string receipt;
        }

        [Serializable]
        public class IOSPurchase
        {
            public string userGuid;
            public string receipt;
        }
    }
}
