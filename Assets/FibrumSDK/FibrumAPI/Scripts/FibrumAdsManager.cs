﻿using System;
using System.IO;
using BestHTTP;
using LitJson;
using UnityEngine;

namespace Assets.FibrumAPI.Scripts
{
    public class FibrumAdsManager : MonoBehaviour {

        public static FibrumAdsManager Instance;
        public Texture2D closeButtonTexture;
        public EventHandler OnInitializeComplete;
        public EventHandler<BannerLoadEventArgs> BannerLoadedEvent;
        public Banner CurrentBanner;

        public struct BannerRequestData
        {
            public float downloadedTime;
            public float requestStartedTime;
            public float bannerShownTime;
            public int requestBPlaceID;
        }

        public enum CrossPromoEvent
        {
            BannerServerResponse = 13,
            Requested = 14,
            Received = 15,
            Decodeerror = 16,
            Downloaderror = 17,
            Shown = 18,
            Clicked = 19,
            Timeout = 20,
            Closed = 21,
            Downloaded = 22
        }

        public class BannerLoadEventArgs : EventArgs
        {
            public Banner LoadedBanner { get; set; }
        }

        void Awake()
        {
            if (Instance == null) Instance = this;
            else if (Instance != this) Destroy(gameObject.GetComponent(Instance.GetType()));
            DontDestroyOnLoad(gameObject);
        }

        public enum BannerType
        {
            FullscreenImage,
            FullscreenVideo,
            WindowedImage,
            WindowedVideo,
        }
    
        public class Banner
        {
            public BannerRequestData RequestData;
            public BannerType Type;
            public string LinkURI;
            public string ContentURI;
            public string BannerSessionID;
			public string BannerID;
        }

        public class FullscreenImageBanner : Banner
        {
            public Texture2D BannerImage;
            public FullscreenImageBanner(Banner bannerToCopy)
            {
                this.RequestData = bannerToCopy.RequestData;
                this.Type = BannerType.FullscreenImage;
            }
        }

        public class FullscreenVideoBanner : Banner
        {
            public string FilePath;
            public FullscreenVideoBanner(Banner bannerToCopy)
            {
                this.RequestData = bannerToCopy.RequestData;
                this.Type = BannerType.FullscreenVideo;
            }
        }

        public void RequestBanner(int bPlaceID)
        {
            CurrentBanner = new Banner
            {
                RequestData =
                {
                    requestBPlaceID = bPlaceID,
                    requestStartedTime = Time.time
                }
            };

            string[] eventParams = new string[10];
            eventParams[0] = bPlaceID.ToString();
            FibrumAPIEvents.Instance.SendCustomEvent((int)CrossPromoEvent.Requested, eventParams, string.Empty);

            FibrumAPI.Instance.StartCoroutine(FibrumAPI.Instance.GetBannerRequest(bPlaceID, OnBannerRequestSuccess, OnBannerRequestFail));
        }

        public void OnBannerLoadedEvent(Banner banner)
        {
            if (BannerLoadedEvent != null)
            {
                BannerLoadedEvent(this, new BannerLoadEventArgs {LoadedBanner = banner});
            }
        }

        public void OnBannerContentRequestSuccess(HTTPRequest request)
        {
            switch (CurrentBanner.Type)
            {
                case BannerType.FullscreenImage:
                    try
                    {
                        ((FullscreenImageBanner)CurrentBanner).BannerImage = request.Response.DataAsTexture2D;

                        CurrentBanner.RequestData.downloadedTime = Time.time;

                        string[] eventParams = new string[10];
					eventParams[0] = CurrentBanner.RequestData.requestBPlaceID.ToString();
					eventParams[1] = CurrentBanner.BannerID.ToString();
					eventParams[2] = (Time.time - CurrentBanner.RequestData.requestStartedTime).ToString();
					eventParams [3] = CurrentBanner.BannerSessionID.ToString();
					FibrumAPIEvents.Instance.SendCustomEvent((int)CrossPromoEvent.Downloaded, eventParams, CurrentBanner.BannerSessionID);
                    }
                    catch (Exception)
                    {
                        string[] eventParams = new string[10];
					eventParams[0] = CurrentBanner.RequestData.requestBPlaceID.ToString();
					eventParams[1] = CurrentBanner.BannerID.ToString();
					eventParams [3] = CurrentBanner.BannerSessionID.ToString ();
					FibrumAPIEvents.Instance.SendCustomEvent((int)CrossPromoEvent.Decodeerror, eventParams, string.Empty);
                        CurrentBanner = null;
                        LoadNextLevel();
                    }
                    break;
                case BannerType.FullscreenVideo:
                    string filename = Guid.NewGuid() + ".mp4";
                    string path = Application.persistentDataPath + "/" + filename;
                    File.WriteAllBytes(path, request.Response.Data);
                    ((FullscreenVideoBanner)CurrentBanner).FilePath = path;
                    break;
            }
            OnBannerLoadedEvent(CurrentBanner);
        }


        public void OnBannerContentRequestFail(string message)
        {
            string[] eventParams = new string[10];
			eventParams[0] = CurrentBanner.RequestData.requestBPlaceID.ToString();
			eventParams [1] = CurrentBanner.BannerID.ToString ();
			eventParams [3] = CurrentBanner.BannerSessionID.ToString ();
			FibrumAPIEvents.Instance.SendCustomEvent((int)CrossPromoEvent.Downloaderror, eventParams, CurrentBanner.RequestData.requestBPlaceID.ToString());
            CurrentBanner = null;
            LoadNextLevel();
        }

        public void OnBannerRequestSuccess(JsonData data)
        {
            string type = data["data"]["type"].ToString();
            string linkURI = data["data"]["linkURI"].ToString();
            string contentURI = data["data"]["contentURI"].ToString();
            string sessionID = data["data"]["adSessionId"].ToString();
			string bannerID;
			try
			{
				bannerID = data ["data"] ["bannerid"].ToString ();
			}
			catch
			{
				bannerID = "Unknown";
			}

            string[] eventParams = new string[10];
            eventParams[0] = CurrentBanner.RequestData.requestBPlaceID.ToString();
            eventParams[2] = (Time.time - CurrentBanner.RequestData.requestStartedTime).ToString();
            FibrumAPIEvents.Instance.SendCustomEvent((int)CrossPromoEvent.Received, eventParams, sessionID);

            switch (type)
            {
                case "image":
                    CurrentBanner = new FullscreenImageBanner(CurrentBanner);
                    break;
                case "video":
                    CurrentBanner = new FullscreenVideoBanner(CurrentBanner);
                    break;
            }

            CurrentBanner.LinkURI = linkURI;
            CurrentBanner.ContentURI = contentURI;
            CurrentBanner.BannerSessionID = sessionID;
			CurrentBanner.BannerID = bannerID;

            FibrumAPI.Instance.StartCoroutine(FibrumAPI.Instance.BannerContentRequest(CurrentBanner, OnBannerContentRequestSuccess, OnBannerContentRequestFail));
        }


        public void OnBannerRequestFail(string message)
        {
            string[] eventParams = new string[10];
			eventParams[0] = CurrentBanner.RequestData.requestBPlaceID.ToString();
			FibrumAPIEvents.Instance.SendCustomEvent((int)CrossPromoEvent.Downloaderror, eventParams, string.Empty);
            CurrentBanner = null;
            LoadNextLevel();
        }

        public void LoadNextLevel()
        {
            if (!PlayerPrefs.HasKey("LoaderDisplayed"))
            {
                PlayerPrefs.SetInt("LoaderDisplayed", 2);
                Application.LoadLevel(Application.loadedLevel + 1);
            }
            else
            {
                int loaderDisplayed = PlayerPrefs.GetInt("LoaderDisplayed");
                if (loaderDisplayed > 0)
                {
                    PlayerPrefs.SetInt("LoaderDisplayed", --loaderDisplayed);
                    Application.LoadLevel(Application.loadedLevel + 1);
                }
                else
                {
                    Application.LoadLevel(Application.loadedLevel + 2);
                }
            }
        }

    }
}
