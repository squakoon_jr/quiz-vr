﻿using System;
using BestHTTP;
using UnityEngine;

namespace Assets.FibrumAPI.Scripts
{
    public static class FibrumAPIDebug
    {
        public static bool DEBUG = true;

        public static void LogMessage(object obj, string message)
        {
            if (DEBUG) Debug.Log(string.Format("{0}:{1}", obj.GetType().Name, message));
        }

        public static void DebugHTTPResponse(HTTPResponse response, string path)
        {
            if (DEBUG)
            {
                Debug.Log(string.Format("FibrumAPI:{0}:Message:{1}", path, response.Message));
                Debug.Log(string.Format("FibrumAPI:{0}:Status Code:{1}", path, response.StatusCode));
                Debug.Log(string.Format("FibrumAPI:{0}:Downloaded Data:{1}", path, response.DataAsText));
            }
        }

        public static void DebugHTTPException(Exception e, string path)
        {
            if (DEBUG)
            {
                Debug.Log(string.Format("FibrumAPI:Error processing {0}:{1}", path, e.Message));
            }
        }

    }
}
