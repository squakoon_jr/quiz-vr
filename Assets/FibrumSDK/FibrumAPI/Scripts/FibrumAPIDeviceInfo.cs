﻿using System;

namespace Assets.FibrumAPI.Scripts
{
    public static class FibrumAPIDeviceInfo
    {

        [Serializable]
        public class SigninData
        {
            public string email;
            public string password;
            public string deviceToken;
        }

        [Serializable]
        public class DeviceInfo
        {
            public InstallationInfo device;
        }

        [Serializable]
        public class InstallationInfo
        {
            public DeviceOS os;
            public DeviceResolution resolution;
            public DeviceSupports supports;
            public DeviceMemory memory;
            public DeviceDatetime datetime;
            public DeviceApp app;

            public InstallationInfo()
            {
                os = new DeviceOS();
                resolution = new DeviceResolution();
                supports = new DeviceSupports();
                memory = new DeviceMemory();
                datetime = new DeviceDatetime();
                app = new DeviceApp();
            }

            [Serializable]
            public class DeviceOS
            {
                public string name;
            }

            [Serializable]
            public class DeviceResolution
            {
                public int height;
                public int width;
            }

            [Serializable]
            public class DeviceSupports
            {
                public bool accelerometer;
                public bool gyroscope;
            }

            [Serializable]
            public class DeviceMemory
            {
                public int graphicMemorySize;
                public int maxTextureSize;
                public int systemMemorySize;
            }

            [Serializable]
            public class DeviceDatetime
            {
                public string timezone;
            }

            [Serializable]
            public class DeviceApp
            {
                public string id;
                public string name;
                public string version;
            }
        }
    }
}