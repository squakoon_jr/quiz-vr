﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Assets.FibrumAPI.Scripts.FibrumLocalization;
using Assets.FibrumAPI.Scripts.UI;
using BestHTTP;
using LitJson;
using UnityEngine;

namespace Assets.FibrumAPI.Scripts
{
    public class FibrumAPI : MonoBehaviour
    {
        public int PCodeHoursLeft;
        public bool PCodeActive;
        public string PCodeValidationStamp;

        public string TokenDir;
        public string PrefsHDir;
        public static FibrumAPI Instance;
        public static bool NetworkAvailable { get; private set; }

        public bool Initialized { get; private set; }
        public string FullGameInApp { get; private set; }
        public int DiscountPercent { get; private set; }
        public int PercentFillProfile { get; private set; }
        public bool TrialEndReached { get; private set; }
        public string FullGamePrice;

        [Tooltip("Find in FibrumSDK/AdditionalPlugins/VoxelBusters/NativePlugins/Prefab/WebViewController.prefab")]
        public GameObject webViewController_iOS;
        //private DateTime serverTime { get; set; }
        //private float startTimeOffset;
        //public DateTime ClientRealTime
        //{
        //    get { return serverTime.Add(TimeSpan.FromSeconds(Time.realtimeSinceStartup-startTimeOffset));}
        //}



#if UNITY_IOS



        public string groupAccessId = "VMQN9AAD4C.com.fibrumApp";

       

        private string KeyChain
        {
            get
            {
                //KeyChainBinding.DeleteKeyChainData();
                string key = "data";
                string value;

                //  KeyChainBinding.GetKeyChainData(out key, out value);
                KeyChainBinding.GetKeyChainData_AG(out key, out value, groupAccessId);
                if (FibrumAPIDebug.DEBUG)   FibrumAPIDebug.LogMessage(this, "SETTER FIRST GET KeyChain: " + value);
                //FibrumAPIDebug.LogMessage(this, "Get KeyChain: " + value);
                return value;
            }
            set
            {
                string keyChainData;
                string key;

                KeyChainBinding.GetKeyChainData_AG(out key, out keyChainData, groupAccessId);
                if (FibrumAPIDebug.DEBUG)   FibrumAPIDebug.LogMessage(this, "GETTER FIRST GET KeyChain: " + keyChainData);
                if (!string.IsNullOrEmpty(keyChainData))
                {
                    if (!keyChainData.Contains(Application.bundleIdentifier))
                    {
                        keyChainData += ";" + value;
                        //  KeyChainBinding.SetKeyChainData("data", keyChainData);
                        KeyChainBinding.ag_SetKeyChainData("data", keyChainData, groupAccessId);
                    }
                    else
                    {
                        var temp = string.Empty;
                        var result = string.Empty;
                        var splitKeyChain = keyChainData.Split(';');
                        var index = 0;
                        for (int i = 0; i < splitKeyChain.Length; i++)
                        {
                            if (splitKeyChain[i].Contains(value.Split(':')[0]) && !splitKeyChain[i].Contains(value.Split(':')[1]))
                            {
                                temp = Application.bundleIdentifier + ":" + value.Split(':')[1];
                                index = i;
                            }

                        }

                        if (index != 0)
                            splitKeyChain[index] = temp;

                        for (int k = 0; k < splitKeyChain.Length; k++)
                        {
                            if (k == splitKeyChain.Length - 1)
                            {
                                result += splitKeyChain[k];
                                continue;
                            }
                            result += splitKeyChain[k] + ";";

                            FibrumAPIDebug.LogMessage(this, "CHECK write KeyChain: " + result);
                        }
                        /*foreach (var splitKey in splitKeyChain)
					{						
						result += splitKey + ";";                    
					}*/

                        //	KeyChainBinding.SetKeyChainData("data",  result);
                        KeyChainBinding.ag_SetKeyChainData("data", result, groupAccessId);
                         if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "PRE write KeyChain: " + result);
                        string test;
                        KeyChainBinding.GetKeyChainData_AG(out key, out test, groupAccessId);
                         if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "TEST Get KeyChain: " + test);
                    }
                }
            }
        }

#endif

        public bool FullProfile
        {
            get { return PercentFillProfile == 100; }
        }

        public string PremiumCodeExpiryDate
        {
            get
            {
                //if (debugPremiumCode) return "2016-07-14T17:41:09.106+0000";
                return PlayerPrefs.GetString("PremiumCodeExpiryDate", "");
            }
            private set
            {
                PlayerPrefs.SetString("PremiumCodeExpiryDate", value);
                UpdatePrefsHash();
            }
        }

        public string UserName
        {
            get
            {
                return PlayerPrefs.GetString("UserName", "");
            }
            private set
            {
                PlayerPrefs.SetString("UserName", value);
            }
        }

        public string PCodeValidationTimestamp
        {
            get
            {
                return PlayerPrefs.GetString("PremiumCodeValidationTimestamp", "");
            }
            private set
            {
                PlayerPrefs.SetString("PremiumCodeValidationTimestamp", value);
                UpdatePrefsHash();
            }
        }

        public string CurrentUserStatus
        {
            get { return PlayerPrefs.GetString("UserStatus", ""); }
            private set
            {
                PlayerPrefs.SetString("UserStatus", value);
                UpdatePrefsHash();
            }
        }

        public bool IsDeviceConfirmed
        {
            get { return CurrentDeviceStatus == "confirmed"; }
        }

        public string CurrentDeviceStatus
        {
            get { return PlayerPrefs.GetString("DeviceStatus", ""); }
            private set
            {
                PlayerPrefs.SetString("DeviceStatus", value);
                UpdatePrefsHash();
            }
        }

        //public bool debugUserConfirmed = true;
        public bool IsUserConfirmed
        {
            get { return CurrentUserStatus == "confirm"; }
        }

        public bool IsPurchased
        {
            get { return PlayerPrefs.GetString("PayStatus", "") == "purchased"; }
        }

        public int PremiumCodeHoursLeft
        {
            get { return GetPremiumCodeTimeLeft(); }
        }

        public bool PremiumCodeActive
        {
            get 
            { 
                bool premium = GetPremiumCodeTimeLeft () > 0;

                {
                    string key = "Events_PremiumEnd";

                    if (PlayerPrefs.HasKey (key))
                    {
                        bool value = bool.Parse (PlayerPrefs.GetString (key));

                        if (!premium && value)
                        {
                            PlayerPrefs.SetString (key, premium.ToString ());

                            string[] eventParams = new string[10];

                            eventParams [0] = "FullVersion";
                            eventParams [1] = "End";

                            FibrumAPIEvents.Instance.SendCustomEvent (FibrumAPIEvents.EventType.Common, eventParams, string.Empty);
                        }
                    } 
                    else
                    {
                        PlayerPrefs.SetString (key, premium.ToString ());
                    }
                }

                return premium; 
            }
        }

        public bool TrialActive
        {
            get
            {
                if (AppFreemiumType == FreemiumType.TriesCount)
                {
                    return Trials.Count <= 0 || Trials.Values.Any(a => a >= 0);
                }
                return !TrialEndReached;
            }
        }

        private Dictionary<string, int> availableAppTrials { get; set; }

        public Dictionary<string, int> Trials
        {
            get { return ParseTrialsData(PlayerPrefs.GetString("Trials", "")); }
            private set
            {
                PlayerPrefs.SetString("Trials", WriteTrialsData(value));
                UpdatePrefsHash();
            }
        }

        public string InAppProducts
        {
            get { return PlayerPrefs.GetString("InAppProducts", ""); }
            private set { PlayerPrefs.SetString("InAppProducts", value); }
        }

        public List<string> InAppProductIDs; 

        public void WriteInAppProductsData(string inappID, bool status)
        {
            JsonData data = JsonMapper.ToObject(InAppProducts);

            for (int i = 0; i < data.Count; i++)
            {
                if (data[i]["product_id"].ToString() == inappID)
                {
                    data[i]["paid"] = status;
                }
            }
            InAppProducts = JsonMapper.ToJson(data);
        }

        public bool GetInAppProductStatus(string inappID)
        {
            JsonData data = JsonMapper.ToObject(InAppProducts);

            for (int i = 0; i < data.Count; i++)
            {
                if (data[i]["product_id"].ToString() == inappID)
                {
                    return bool.Parse(data[i]["paid"].ToString());
                }
            }
            return false;
        }

        private string WriteTrialsData(Dictionary<string, int> trials)
        {
            Debug.Log("Trials json:" + JsonMapper.ToJson(trials));
            return JsonMapper.ToJson(trials);
        }

        private Dictionary<string, int> ParseTrialsData(string trialsData)
        {
            JsonData data = JsonMapper.ToObject(trialsData);
            Dictionary<string, int> appTrials = new Dictionary<string, int>();

            for (int i = 0; i < data.Count; i++)
            {
                appTrials.Add(data.Keys.ToArray()[i], (int)data[i]);
            }
            return appTrials;
        }

        public void SetTrialsData(string trialTypeKey, int trialTypeValue)
        {
            if (!Trials.ContainsKey(trialTypeKey))
            {
                Debug.Log("No such TrialTypeKey found");
                return;
            }
            Dictionary<string, int> tempTrials = Trials.ToDictionary(entry => entry.Key, entry => entry.Value);
            tempTrials[trialTypeKey] = trialTypeValue;
            Trials = tempTrials.ToDictionary(entry => entry.Key, entry => entry.Value);
            OnTrialChanged();
            CheckIsApplicationAvailable();
        }

        public string ApplicationName;
        public string ClientID;

        [SerializeField]
        private string clientSecret;
        private int currentTimestamp;

        public enum FreemiumType
        {
            TriesCount,
            TrialLevel,
            CustomInApps
        };

        public FreemiumType AppFreemiumType;

        public enum OSNames
        {
            ios,
            android
        }

        private const string serverURL = "https://api.fibrum.com";
        private const string UserInfoPath = "/v3/users";
        private const string SignupSilentPath = "/v3/users/signin/device";
        private const string InstallationsPath = "/v3/installations";
        private const string EventPath = "/v3/events";
        private const string EventPathBatch = "/v3/events/batch";
        private const string ReceiptsGooglePath = "/v3/payments/receipts/google";
        private const string ReceiptsApplePath = "/v3/payments/receipts/apple";
        private const string AppPaymentsPath = "/v3/payments/installations";
        private const string AppInfoPath = "/v3/apps";
        private const string BannersPath = "/v3/banners";
        private const string PingPath = "/v3/_ping";
        private const string TimeSyncPath = "/v3/_time";
        private string InstallationsGetTokenPath = "";
        private string URLInstallationsGetToken;

        public static string DeviceToken;
        public static string UserGuid;
        public static string InstallationGuid;
        public static string SessionGuid;
        public static string AppGuid;

        private string tokenpath;
        private string prefshashpath;
        private LocalizationController _localizationController;

        //private bool lockEventsWrite;
        //public List<FibrumAPIEvents.AppCustomEvent> tempEventBatchList = new List<FibrumAPIEvents.AppCustomEvent>();
        public event EventHandler<AppBlockEventArgs> BlockApplication;
        public event EventHandler<UserPurchasesEventArgs> GetUserPurchasesProcessed;
        public event EventHandler NetworkStatusChecked;
        public static event EventHandler InitializedEvent;
        public static event EventHandler<InitializationProgressEventArgs> InitializationProgressEvent;
        public static event EventHandler TrialChanged;

        public class AppBlockEventArgs : EventArgs
        {
            public enum BlockEventType
            {
                TrialEnd,
                NetworkUnavailable,
                WaitingUserConfirm,
                HTTPRequestFail,
                TimeSyncFail
            }
            public BlockEventType AppBlockType { get; set; }
        }

        public class UserPurchasesEventArgs : EventArgs
        {
            public bool IsPurchased { get; set; }
        }

        public class InitializationProgressEventArgs : EventArgs
        {
            public float Progress { get; set; }
        }

        public class NetworkStatusEventArgs : EventArgs
        {
            public bool IsAvailable { get; set; }
        }

        private int GetPremiumCodeTimeLeft()
        {
            if (string.IsNullOrEmpty(PremiumCodeExpiryDate)) return 0;
            DateTime date = DateTime.Parse(PremiumCodeExpiryDate);
            int hoursLeft = (int)(date.ToUniversalTime().Subtract(DateTime.UtcNow)).TotalHours;
            if (hoursLeft <= 0) PremiumCodeExpiryDate = "";
            Debug.Log("Premium Code hours left:" + hoursLeft);
            return hoursLeft;
        }

        private bool CompareDateTime(string serverDateTime)
        {
            Debug.Log("CompareDateTime:serverDateTime:" + serverDateTime);
            DateTime date = DateTime.Parse(serverDateTime);
            int mins = (date.ToUniversalTime().Subtract(DateTime.UtcNow)).Minutes;
            Debug.Log("CompareDateTime:Time difference:" + mins);
            return Mathf.Abs(mins) < 3;
        }

        private bool ValidatePremiumCodeTimestamp()
        {
            if (!PremiumCodeActive) return true;
            Debug.Log("ValidatePremiumCodeTimestamp: pcode validation timestamp:" + PCodeValidationTimestamp);
            DateTime timestamp = DateTime.Parse(PCodeValidationTimestamp);
            Debug.Log("CurrentTime:" + DateTime.UtcNow.ToUniversalTime());
            Debug.Log("ValidatePremiumCodeTimestamp:Time difference:" + (DateTime.UtcNow.ToUniversalTime() - timestamp));
            return (DateTime.UtcNow.ToUniversalTime() > timestamp);
        }

        private void SetPurchaseStatus(bool purchased)
        {
            if(purchased)
            {
                string[] eventParams = new string[10];
                eventParams[0] = "FullVersion";           
                FibrumAPIEvents.Instance.SendCustomEvent(36, eventParams, string.Empty);
           
                eventParams = new string[10];
                eventParams[0] = "FullVersion";
                eventParams[1] = "Start";
                FibrumAPIEvents.Instance.SendCustomEvent(FibrumAPIEvents.EventType.Common, eventParams, string.Empty);
            }

            if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "App status:purchased:" + purchased);
            PlayerPrefs.SetString("PayStatus", purchased ? "purchased" : "");
            UpdatePrefsHash();
        }

        private bool CheckPrefsHash()
        {
            bool isValid = false;
            var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(DeviceToken));
            string checkvalue =
                PlayerPrefs.GetString("PremiumCodeValidationTimestamp", "") +
                PlayerPrefs.GetString("DeviceStatus", "") +
                PlayerPrefs.GetString("UserStatus", "") +
                PlayerPrefs.GetString("Trials", "") +
                PlayerPrefs.GetString("PayStatus", "") +
                PlayerPrefs.GetString("PremiumCodeExpiryDate", "") +
                PlayerPrefs.GetString("UserGUID", "") +
                PlayerPrefs.GetString("InstallationGUID", "") +
                PlayerPrefs.GetString("AppGUID", "") +
                DeviceToken;
            byte[] checksum = hmac.ComputeHash(Encoding.UTF8.GetBytes(checkvalue));
            bool check = false;
#if UNITY_ANDROID
            check = (Convert.ToBase64String(checksum) == File.ReadAllText(prefshashpath, Encoding.UTF8));
#endif

#if UNITY_IOS
            var temp = (Convert.ToBase64String(checksum));
            check = (KeyChain.Contains(temp));
#endif
            if (check)
            {
                if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Valid PlayerPrefs data");
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Valid PlayerPrefs data");
                isValid = true;
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Invalid PlayerPrefs data");
            }
            return isValid;
        }

        private void UpdatePrefsHash()
        {
            if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Writing prefs hash");
            var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(DeviceToken));
            string hashdata =
                PlayerPrefs.GetString("PremiumCodeValidationTimestamp", "") +
                PlayerPrefs.GetString("DeviceStatus", "") +
                PlayerPrefs.GetString("UserStatus", "") +
                PlayerPrefs.GetString("Trials", "") +
                PlayerPrefs.GetString("PayStatus", "") +
                PlayerPrefs.GetString("PremiumCodeExpiryDate", "") +
                UserGuid +
                InstallationGuid +
                AppGuid +
                DeviceToken;
            byte[] checksum = hmac.ComputeHash(Encoding.UTF8.GetBytes(hashdata));
#if UNITY_ANDROID
            File.WriteAllText(prefshashpath, Convert.ToBase64String(checksum), Encoding.UTF8);
#endif

#if UNITY_IOS
            /*************IOS KEYCHAIN**************************/

            string data = Application.bundleIdentifier + ":" + Convert.ToBase64String(checksum);//+ ";"; 

            KeyChain = data;
            /**************************************************/
#endif
            PlayerPrefs.Save();
            if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Writing prefs hash:" + Convert.ToBase64String(checksum));
        }

        private void OnInitializationProgressEvent(float progress)
        {
            if (InitializationProgressEvent != null)
            {
                InitializationProgressEvent(this, new InitializationProgressEventArgs() { Progress = progress });
            }
        }

        private IEnumerator CheckDeviceToken()
        {
            OnInitializationProgressEvent(0.1f);
            tokenpath = TokenDir + ".dtoken.dat";
            prefshashpath = PrefsHDir + string.Format(".{0}.dat", ClientID);
            ;
            bool check = false;
#if UNITY_ANDROID
            check = (File.Exists(tokenpath));
#endif

#if UNITY_IOS
            check = (!string.IsNullOrEmpty(KeyChain.Split(';')[0]));
#endif
            if (check)
            {
                if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Device token exists, checking values");
                DeviceToken = ReadDeviceToken(tokenpath);
                OnInitializationProgressEvent(0.2f);
                if (
#if UNITY_ANDROID
                    File.Exists(prefshashpath) &&
#endif
                        CheckPrefsHash() && ValidatePremiumCodeTimestamp()
                    )
                {
                    OnInitializationProgressEvent(0.3f);
                    if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "PlayerPrefs values OK");

                    UserGuid = PlayerPrefs.GetString("UserGUID");
                    InstallationGuid = PlayerPrefs.GetString("InstallationGUID");
                    AppGuid = PlayerPrefs.GetString("AppGUID");

                    if (NetworkAvailable)
                    {

                        HttpRequestChain chain = gameObject.AddComponent<HttpRequestChain>();
                        chain.Source = this;
                        chain.RequestChain = new List<ChainItem>();


                        chain.RequestChain.Add(new ChainItem(UpdateRegistrationInfoRequest, () =>
                        {
                            OnInitializationProgressEvent(0.4f);
                            Debug.Log("UpdateRegistrationInfoRequest: Success");
                        },
                            () =>
                            {
                                Debug.Log("UpdateRegistrationInfoRequest: Failed");
                                OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                            }));

                        chain.RequestChain.Add(new ChainItem(UserInfoRequest, () =>
                        {
                            OnInitializationProgressEvent(0.5f);
                            Debug.Log("UserInfoRequest: Success");
                        },
                            () =>
                            {
                                Debug.Log("UserInfoRequest: Failed");
                                OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                            }));

                        chain.RequestChain.Add(new ChainItem(GetPremiumCodeInfo, () =>
                        {
                            OnInitializationProgressEvent(0.6f);
                            Debug.Log("GetPremiumCodeInfo: Success");
                        },
                            () =>
                            {
                                Debug.Log("GetPremiumCodeInfo: Failed");
                                OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                            }));

                        if (AppFreemiumType == FreemiumType.TriesCount || AppFreemiumType == FreemiumType.TrialLevel)
                        {
                            chain.RequestChain.Add(new ChainItem(GetAvailableAppProducts, () =>
                            {
                                OnInitializationProgressEvent(0.8f);
                                Debug.Log("GetAvailableAppProducts: Success");
                            },
                                () =>
                                {
                                    Debug.Log("GetAvailableAppProducts: Failed");
                                    OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                                }));
                        }

                        chain.RequestChain.Add(new ChainItem(GetUserPurchases, () =>
                        {
                            OnInitializationProgressEvent(0.9f);
                            Debug.Log("GetUserPurchases: Success");
                        },
                            () =>
                            {
                                Debug.Log("GetUserPurchases: Failed");
                                OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                            }));

                        chain.StartCoroutine(chain.StartChain(() =>
                        {
                            OnInitializedEvent();
                        }, 
                        () => { }));
                    }
                    else
                    {
                        OnInitializedEvent();
                    }
                }
                else
                {
                    if (NetworkAvailable)
                    {
                        if (FibrumAPIDebug.DEBUG)
                            FibrumAPIDebug.LogMessage(this,
                                "PlayerPrefs values not found or invalid, setting up new installation");
                        PlayerPrefs.DeleteAll();
                        yield return StartCoroutine(Install(true));
                    }
                    else
                    {
                        if (FibrumAPIDebug.DEBUG)
                            FibrumAPIDebug.LogMessage(this,
                                "PlayerPrefs values not found or invalid, network unavailable, blocking app");
                        PlayerPrefs.DeleteAll();
                        OnBlockApplication(AppBlockEventArgs.BlockEventType.NetworkUnavailable);
                    }
                }
            }
            else
            {
                if (NetworkAvailable)
                {
                    if (FibrumAPIDebug.DEBUG)
                        FibrumAPIDebug.LogMessage(this, "Device token not found, setting up new installation");
                    PlayerPrefs.DeleteAll();
                    yield return StartCoroutine(Install(false));
                }
                else
                {
                    if (FibrumAPIDebug.DEBUG)
                        FibrumAPIDebug.LogMessage(this, "Device token not found, network unavailable, blocking app");
                    PlayerPrefs.DeleteAll();
                    OnBlockApplication(AppBlockEventArgs.BlockEventType.NetworkUnavailable);
                }
            }
            Initialized = true;
        }

        public void OnInitializedEvent()
        {
            PCodeHoursLeft = PremiumCodeHoursLeft;
            PCodeActive = PremiumCodeActive;
            PCodeValidationStamp = PCodeValidationTimestamp;

            OnInitializationProgressEvent(1f);

            FibrumAPIEvents.Instance.StartSessionEvent();
            CheckIsGamepadAvailable ();

            if (NetworkAvailable && !IsPurchased && !PremiumCodeActive)
            {
                Application.LoadLevel(Application.loadedLevel + 1);
            }
            else
            {
                if (!PlayerPrefs.HasKey("LoaderDisplayed"))
                {
                    PlayerPrefs.SetInt("LoaderDisplayed", 2);
                    Application.LoadLevel(Application.loadedLevel + 2);
                }
                else
                {
                    int loaderDisplayed = PlayerPrefs.GetInt("LoaderDisplayed");
                    if (loaderDisplayed > 0)
                    {
                        PlayerPrefs.SetInt("LoaderDisplayed", --loaderDisplayed);
                        Application.LoadLevel(Application.loadedLevel + 2);
                    }
                    else
                    {
                        Application.LoadLevel(Application.loadedLevel + 3);
                    }
                }
            }

            if (InitializedEvent != null)
            {
                InitializedEvent(this, new EventArgs());
            }
        }

        protected void OnTrialChanged()
        {
            if (TrialChanged != null) TrialChanged(this, EventArgs.Empty);
        }

        public bool CheckIsApplicationAvailable()
        {
            if (IsPurchased || TrialActive || PremiumCodeActive || AppFreemiumType == FreemiumType.CustomInApps) return true;
            OnBlockApplication(AppBlockEventArgs.BlockEventType.TrialEnd);
            return false;
        }

        public bool CheckIsApplicationAvailable_My()
        {
            if (IsPurchased  || PremiumCodeActive) return true;
           // OnBlockApplication(AppBlockEventArgs.BlockEventType.TrialEnd);
            return false;
        }

        void CheckIsGamepadAvailable()
        {
            bool isAvailable = Input.GetJoystickNames ().Length > 0;

            string[] eventParams = new string[10];

            eventParams [0] = isAvailable ? "Yes" : "No";

            FibrumAPIEvents.Instance.SendCustomEvent (FibrumAPIEvents.EventType.Gamepad, eventParams, string.Empty);

            if (FibrumAPIDebug.DEBUG) 
                FibrumAPIDebug.LogMessage(this, "Gamepad availability: " + isAvailable.ToString());
        }

        private IEnumerator SwitchAppFreemiumType(Action onDone, Action onFail)
        {
            switch (AppFreemiumType)
            {
                case FreemiumType.TriesCount:
                    yield return StartCoroutine(HandleAppTrials(
                        () =>
                        {
                            StartCoroutine(GetAvailableAppProducts(
                                onDone,
                                () =>
                                {
                                    OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                                }));
                        },
                        () =>
                        {
                            OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                        }
                        ));
                    break;
                case FreemiumType.TrialLevel:
                    yield return StartCoroutine(GetAvailableAppProducts(onDone,
                        () => { OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail); }));
                    break;
                case FreemiumType.CustomInApps:
                    yield return null;
                    onDone();
                    break;
            }
        }

        private IEnumerator HandleAppTrials(Action onSuccess, Action onFail)
        {
            yield return StartCoroutine(AppInfoRequest(
                () =>
                {
                    StartCoroutine(SendAppTrialsConsumed(onSuccess, onFail));
                }, onFail));
        }

        private IEnumerator Install(bool includeToken)
        {
            yield return StartCoroutine(RegistrationRequest(includeToken,
                () =>
                {
                    HttpRequestChain chain = gameObject.AddComponent<HttpRequestChain>();
                    chain.Source = this;
                    chain.RequestChain = new List<ChainItem>();

                    chain.RequestChain.Add(new ChainItem(DeviceTokenRequest, () =>
                    {
                        OnInitializationProgressEvent(0.2f);
                        Debug.Log("DeviceTokenRequest: Success");
                    },
                        () =>
                        {
                            Debug.Log("DeviceTokenRequest: Failed");
                            OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                        }));

                    chain.RequestChain.Add(new ChainItem(SwitchAppFreemiumType, () =>
                    {
                        OnInitializationProgressEvent(0.3f);
                        Debug.Log("SwitchAppFreemiumType: Success");
                    },
                        () =>
                        {
                            Debug.Log("SwitchAppFreemiumType: Failed");
                            OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                        }));

                    chain.RequestChain.Add(new ChainItem(UserInfoRequest, () =>
                    {
                        OnInitializationProgressEvent(0.4f);
                        Debug.Log("UserInfoRequest: Success");
                    },
                        () =>
                        {
                            Debug.Log("UserInfoRequest: Failed");
                            OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                        }));

                    chain.RequestChain.Add(new ChainItem(GetPremiumCodeInfo, () =>
                    {
                        OnInitializationProgressEvent(0.7f);
                        Debug.Log("GetPremiumCodeInfo: Success");
                    },
                        () =>
                        {
                            Debug.Log("GetPremiumCodeInfo: Failed");
                            OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                        }));

                    chain.RequestChain.Add(new ChainItem(GetUserPurchases, () =>
                    {
                        OnInitializationProgressEvent(0.9f);
                        Debug.Log("GetUserPurchases: Success");
                    },
                        () =>
                        {
                            Debug.Log("GetUserPurchases: Failed");
                            OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                        }));

                    chain.StartCoroutine(chain.StartChain(() =>
                    {
                        Debug.Log("Install: Success");
                        OnInitializedEvent();
                    }, () => { }));
                },
                () =>
                {
                    OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                    if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Installation request failed!");
                }));
        }

        private IEnumerator SendAppTrialsConsumed(Action onSuccess, Action onFail)
        {
            currentTimestamp = GetTimestamp();
            bool success = false;
            List<string> consumedTrials = new List<string>();
            foreach (var customTrial in availableAppTrials)
            {
                string path = string.Format("/v3/trials/{0}/installation/{1}/consume", customTrial.Key, InstallationGuid);
                var request = new HTTPRequest(new Uri(serverURL + path), HTTPMethods.Post);
                request.SetHeader("Content-Type", "application/json; charset=UTF-8");
                request.AddHeader("Authorization", GetAuthHeaderSignature(string.Empty, "POST", path, string.Empty));
                request.Send();
                yield return StartCoroutine(request);
                if (request.Exception == null)
                {
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, path);

                    if (request.Response.StatusCode != 201)
                    {
                        consumedTrials.Add(customTrial.Key);
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, string.Format("Trial {0} is already consumed", customTrial.Key));
                    }
                    success = true;
                }
                else
                {
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, path);
                    success = false;
                }
            }
            if (success)
            {
                foreach (var trial in consumedTrials)
                {
                    if (availableAppTrials.ContainsKey(trial))
                    {
                        availableAppTrials[trial] = 0;
                    }
                }
                Trials = availableAppTrials.ToDictionary(entry => entry.Key, entry => entry.Value);
                Debug.Log("Trials.Count:" + Trials.Count);
                foreach (var kvpair in Trials)
                {
                    Debug.Log("Trials.Key:" + kvpair.Key + " Trials.Value:" + kvpair.Value);
                }
                onSuccess();
            }
            else
            {
                Debug.Log("SendAppTrialsConsumed failed, fail action");
                onFail();
            }
        }

        private void Awake()
        {
            //#if UNITY_EDITOR && UNITY_IOS
            //            webViewprefab = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>("Assets/FibrumSDK/AdditionalPlugins/VoxelBusters/NativePlugins/Prefab/WebViewController.prefab");
            //            UnityEditor.AssetDatabase.SaveAssets();
            //#endif

#if UNITY_IOS     
            if (webViewController_iOS != null)
            {
                Instantiate(webViewController_iOS);
            }
            else
            {
                Debug.LogError("Lost WebViewPrefab ref  in FibrumAPI prefab.Find in FibrumSDK/AdditionalPlugins/VoxelBusters/NativePlugins/Prefab/WebViewController.prefab");
            }
#endif


            if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Awake");
            if (Instance == null) Instance = this;
            else if (Instance != this) Destroy(gameObject.GetComponent(Instance.GetType()));
            DontDestroyOnLoad(gameObject);
            //FibrumAPIBilling.BillingReady += OnBillingReady;
            if (!Initialized) Init();
            _localizationController = new LocalizationController();
            _localizationController.Init();
        }

        private void BlockAppOnStart(AppBlockEventArgs.BlockEventType blockType)
        {
            switch (blockType)
            {
                case AppBlockEventArgs.BlockEventType.NetworkUnavailable:
                    OnBlockApplication(AppBlockEventArgs.BlockEventType.NetworkUnavailable);
                    break;
                case AppBlockEventArgs.BlockEventType.TimeSyncFail:
                    OnBlockApplication(AppBlockEventArgs.BlockEventType.TimeSyncFail);
                    break;
                case AppBlockEventArgs.BlockEventType.HTTPRequestFail:
                    OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                    break;
            }
        }

        private void GetAndroidRootDirectory()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass Environment = new AndroidJavaClass("android.os.Environment");
            using (
                AndroidJavaObject externalStorageDirectory =
                    Environment.CallStatic<AndroidJavaObject>("getExternalStorageDirectory"))
            {
                string root = externalStorageDirectory.Call<string>("getPath");
                Debug.Log("ExternalStorageDirectory: " + root);
                TokenDir = string.Concat(root, "/.sysdata/");
                PrefsHDir = string.Concat(root, "/.sysdata/");
                Debug.Log("TokenDir: " + TokenDir);
                Debug.Log("PrefsHDir: " + PrefsHDir);
            }
#endif
        }

        public void Init()
        {
            if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Init");
            DontDestroyOnLoad(gameObject);
            if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Getting network availability");
            GetAndroidRootDirectory();
            StartCoroutine(GetNetworkStatus(
                () =>
                {
                    Guid sessionGuid = Guid.NewGuid();
                    SessionGuid = sessionGuid.ToString();
                    StartCoroutine(CheckDeviceToken());
                },
                (blockType) =>
                {
                    BlockAppOnStart(blockType);
                }
            ));
           if(AppFreemiumType==FreemiumType.CustomInApps) Debug.Log("App Products: " + InAppProducts);
        }

        private IEnumerator GetNetworkStatus(Action onComplete, Action<AppBlockEventArgs.BlockEventType> onFail)
        {
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + PingPath), HTTPMethods.Get);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(string.Empty, "GET", PingPath, string.Empty));
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, PingPath);
                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {
                    NetworkAvailable = Convert.ToBoolean((int)data["data"]["ping"]);
                    onComplete();
                }
                else
                {
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Server unavailable");
                    if (data["message"] != null)
                    {
                        string message = data["message"].ToString();

                        switch (message)
                        {
                            case "InvalidSign":
                                onFail(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                                break;
                            case "RequestExpired":
                                onFail(AppBlockEventArgs.BlockEventType.TimeSyncFail);
                                break;
                            default:
                                onFail(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                                break;
                        }
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, message);
                        NetworkAvailable = false;
                    }
                }
            }
            else
            {
                NetworkAvailable = false;
                onComplete();
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, PingPath);
            }
            if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Network available:"+NetworkAvailable);
            OnNetworkStatusChecked();
        }

        //private IEnumerator GetServerTime(Action onComplete)
        //{
        //    var request = new HTTPRequest(new Uri(serverURL + TimeSyncPath), HTTPMethods.Get);
        //    //request.SetHeader("Content-Type", "application/json; charset=UTF-8");
        //    //request.AddHeader("Authorization", GetAuthHeaderSignature(string.Empty, "GET", TimeSyncPath, string.Empty));
        //    request.Send();
        //    yield return StartCoroutine(request);
        //    if (request.Exception == null)
        //    {
        //        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, TimeSyncPath);
        //        JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
        //        if (data["status"].ToString() == "success")
        //        {
        //            serverTime = DateTime.Parse(data["data"]["datetime"].ToString()).ToUniversalTime();
        //            Debug.Log("serverTime:" + serverTime);
        //            startTimeOffset = Time.realtimeSinceStartup;
        //            Debug.Log("startTimeOffset" + startTimeOffset);
        //        }
        //        else
        //        {
        //            if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Server unavailable");
        //        }
        //    }
        //    else
        //    {
        //        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, TimeSyncPath);
        //    }
        //    onComplete();
        //}

        private void OnNetworkStatusChecked()
        {
            if (NetworkStatusChecked != null)
            {
                NetworkStatusChecked(this, new EventArgs());
            }
        }

        private void OnBlockApplication(AppBlockEventArgs.BlockEventType blockEventType)
        {
            Debug.Log("OnBlockApplication");
            if (BlockApplication != null)
            {
                BlockApplication(this, new AppBlockEventArgs() { AppBlockType = blockEventType });
            }
        }

        private void OnUserPurchasesProcessed(bool purchased)
        {
            if (GetUserPurchasesProcessed != null)
            {
                GetUserPurchasesProcessed(this, new UserPurchasesEventArgs() { IsPurchased = purchased });
            }
        }

        private void OnApplicationQuit()
        {
            SetPCodeValidationTimestamp();
        }

        private void SetPCodeValidationTimestamp()
        {
            if (PremiumCodeActive)
            {
                PCodeValidationTimestamp = DateTime.UtcNow.ToUniversalTime().ToString();
                Debug.Log(PCodeValidationTimestamp);
            }
        }

        private void WriteDeviceToken(string path, string token)
        {
#if UNITY_ANDROID
            if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Writing token at path: " + path);
            if (!Directory.Exists(TokenDir))
            {
                Directory.CreateDirectory(TokenDir);
            }
            File.WriteAllText(path, token);
            if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "File writed at path:" + path);
#endif

#if UNITY_IOS
			string key;
			string keyChainData;
			KeyChainBinding.GetKeyChainData_AG(out key,out keyChainData,groupAccessId);
			if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Writing token: " + keyChainData);
			if (string.IsNullOrEmpty(keyChainData))
			{
				KeyChainBinding.ag_SetKeyChainData("data", DeviceToken,groupAccessId);
				return;
			}
#endif
        }

        private string ReadDeviceToken(string path)
        {
#if UNITY_ANDROID
            if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Reading file at path: " + path);
            string[] arrData = File.ReadAllLines(path);
            if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Device token read: " + arrData[0]);
            return arrData[0];
#elif UNITY_IOS

			if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Device token read: " + KeyChain.Split(';')[0]);
            return KeyChain.Split(';')[0];
#else
			return "";
#endif
        }

        public static string GetFormattedW3CDate()
        {
            string formatString = "yyyy-MM-ddTHH:mm:ss.fff";
            return DateTime.UtcNow.ToString(formatString) + "Z";
        }

        private int GetTimestamp()
        {
            return (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        private string GetSignature(string mbody, string httpVerb, string methodPath, string queryStr)
        {
            string query = GetHashMD5(queryStr);
            string body = GetHashMD5(mbody);
            int timestamp = currentTimestamp;
            string strToSign =
                string.Format("clientId={0};HTTPVerb={1};path={2};query={3};body={4};timestamp={5};",
                    ClientID,
                    httpVerb,
                    methodPath,
                    query,
                    body,
                    timestamp
                );
            string signingKey = Convert.ToBase64String(HashHMAC(
                Encoding.UTF8.GetBytes(timestamp.ToString()),
                Encoding.UTF8.GetBytes(httpVerb + clientSecret)
            ));
            string signature =
                Convert.ToBase64String(HashHMAC(Encoding.UTF8.GetBytes(signingKey), Encoding.UTF8.GetBytes(strToSign)));
            return signature;
        }

        private static string GetHashMD5(string data)
        {
            byte[] hash = Encoding.UTF8.GetBytes(data);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hashenc = md5.ComputeHash(hash);
            string result = "";
            foreach (var b in hashenc)
            {
                result += b.ToString("x2");
            }
            return result;
        }

        private static byte[] HashHMAC(byte[] key, byte[] message)
        {
            var hash = new HMACSHA256(key);
            return hash.ComputeHash(message);
        }

        private string GetTimeZoneOffset()
        {
            return TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours.ToString("00") +
                TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Minutes.ToString("00");
        }

        private string GetAuthHeaderSignature(string body, string requestMethod, string path, string query)
        {
            string authHeader = "Signature " +
                string.Format(
                    "clientId=\"{0}\",algorithm=\"{1}\",timestamp=\"{2}\",signature=\"{3}\"",
                    ClientID,
                    "hmac-sha256",
                    currentTimestamp,
                    GetSignature(body, requestMethod, path, query)
                );

            return authHeader;
        }

        public IEnumerator SendCustomEventBatch(string batch, Action onSuccess, Action onFail)
        {
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + EventPathBatch), HTTPMethods.Post);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(batch, "POST", EventPathBatch, string.Empty));
            request.RawData = Encoding.UTF8.GetBytes(batch);
            request.Send();
            FibrumAPIEvents.Instance.LockSendEvents = true;
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, EventPathBatch);
                onSuccess();
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, EventPathBatch);
                onFail();
            }
            FibrumAPIEvents.Instance.LockSendEvents = false;
        }

        public void SetTrialLevelEndReached()
        {
            TrialEndReached = true;
            CheckIsApplicationAvailable();
        }

        private IEnumerator SendCustomEvent(FibrumAPIEvents.AppCustomEvent appEvent, Action onSuccess, Action onFail)
        {
            string jsonStr = JsonUtility.ToJson(appEvent);
            if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, jsonStr);
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + EventPath), HTTPMethods.Post);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(jsonStr, "POST", EventPath, string.Empty));
            request.RawData = Encoding.UTF8.GetBytes(jsonStr);
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, EventPath);
                onSuccess();
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, EventPath);
                onFail();
            }
        }

        private IEnumerator UpdateRegistrationInfoRequest(Action onSuccess, Action onFail)
        {
            currentTimestamp = GetTimestamp();
            FibrumAPIDeviceInfo.InstallationInfo info = new FibrumAPIDeviceInfo.InstallationInfo();
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    info.os.name = OSNames.android.ToString();
                    break;
                case RuntimePlatform.IPhonePlayer:
                    info.os.name = OSNames.ios.ToString();
                    break;
                case RuntimePlatform.OSXEditor:
                    info.os.name = OSNames.ios.ToString();
                    break;
                case RuntimePlatform.WindowsEditor:
                    info.os.name = OSNames.android.ToString();
                    break;
            }
            info.resolution.height = Screen.height;
            info.resolution.width = Screen.width;
            info.supports.accelerometer = SystemInfo.supportsAccelerometer;
            info.supports.gyroscope = SystemInfo.supportsGyroscope;
            info.memory.graphicMemorySize = SystemInfo.graphicsMemorySize;
            info.memory.maxTextureSize = SystemInfo.maxTextureSize;
            info.memory.systemMemorySize = SystemInfo.systemMemorySize;
            info.datetime.timezone = GetTimeZoneOffset();
            info.app.id = Application.bundleIdentifier;
            info.app.name = ApplicationName;
            info.app.version = Application.version;
            FibrumAPIDeviceInfo.DeviceInfo deviceInfo = new FibrumAPIDeviceInfo.DeviceInfo {device = info};
            string jsonStr = JsonUtility.ToJson(deviceInfo);
            var request =
                new HTTPRequest(
                    new Uri(serverURL + InstallationsPath + "/" + InstallationGuid + "?deviceToken=" + DeviceToken),
                    HTTPMethods.Put);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization",
                GetAuthHeaderSignature(jsonStr, "PUT", "/v3/installations" + "/" + InstallationGuid,
                    "deviceToken=" + DeviceToken));
            request.RawData = Encoding.UTF8.GetBytes(jsonStr);
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, InstallationsPath);
                onSuccess();
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, InstallationsPath);
                onFail();
            }
        }

			private IEnumerator DeviceTokenRequest(Action onSuccess, Action onFail)
        {
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(URLInstallationsGetToken), HTTPMethods.Get);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization",
                GetAuthHeaderSignature(string.Empty, "GET", InstallationsGetTokenPath, string.Empty));
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, InstallationsGetTokenPath);
                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {
                    UserGuid = data["data"]["userGuid"].ToString();
                    DeviceToken = data["data"]["deviceToken"].ToString();
                    InstallationGuid = data["data"]["installationGuid"].ToString();
                    AppGuid = data["data"]["applicationGuid"].ToString();
                    WriteDeviceToken(tokenpath, DeviceToken);
                    PlayerPrefs.SetString("UserGUID", UserGuid);
                    PlayerPrefs.SetString("InstallationGUID", InstallationGuid);
                    PlayerPrefs.SetString("AppGUID", AppGuid);

                    //Debug.Log("userGuid = "+ UserGuid + "; "+ "DeviceToken = " + DeviceToken + "; "+ "InstallationGuid = " + InstallationGuid + "; " + "AppGuid = " + AppGuid + "; ");

                    UpdatePrefsHash();
					if(onSuccess!=null) onSuccess();
                }
                else
                {
                    if (data["message"] != null)
                    {
                        string message = data["message"].ToString();
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, message);
						onFail();
                    }
                }
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, InstallationsGetTokenPath);
                onFail();
            }
        }

        private IEnumerator RegistrationRequest(bool includeToken, Action onSuccess,
            Action onFail)
        {
            FibrumAPIDeviceInfo.InstallationInfo info = new FibrumAPIDeviceInfo.InstallationInfo();

            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    info.os.name = OSNames.android.ToString();
                    break;
                case RuntimePlatform.IPhonePlayer:
                    info.os.name = OSNames.ios.ToString();
                    break;
                case RuntimePlatform.WindowsEditor:
                    info.os.name = OSNames.android.ToString();
                    break;
                case RuntimePlatform.OSXEditor:
                    info.os.name = OSNames.ios.ToString();
                    break;
            }

            info.resolution.height = Screen.height;
            info.resolution.width = Screen.width;
            info.supports.accelerometer = SystemInfo.supportsAccelerometer;
            info.supports.gyroscope = SystemInfo.supportsGyroscope;
            info.memory.graphicMemorySize = SystemInfo.graphicsMemorySize;
            info.memory.maxTextureSize = SystemInfo.maxTextureSize;
            info.memory.systemMemorySize = SystemInfo.systemMemorySize;
            info.datetime.timezone = GetTimeZoneOffset();
            info.app.id = Application.bundleIdentifier;
            info.app.name = ApplicationName;
            info.app.version = Application.version;

            FibrumAPIDeviceInfo.DeviceInfo deviceInfo = new FibrumAPIDeviceInfo.DeviceInfo {device = info};

            string jsonStr = JsonUtility.ToJson(deviceInfo);

            currentTimestamp = GetTimestamp();

            var request =
                new HTTPRequest(
                    new Uri(includeToken
                        ? serverURL + InstallationsPath + "?deviceToken=" + DeviceToken
                        : serverURL + InstallationsPath), HTTPMethods.Post);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(jsonStr, "POST", InstallationsPath,
                includeToken ? "deviceToken=" + DeviceToken : string.Empty));
            request.RawData = Encoding.UTF8.GetBytes(jsonStr);
            request.Send();

            Debug.Log(request.CurrentUri);

            yield return StartCoroutine(request);

            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, InstallationsPath);
                if (request.Response.StatusCode == 201)
                {
                    if (request.Response.Headers.ContainsKey("location"))
                    {
                        List<string> lstlocation = request.Response.Headers["location"];
                        URLInstallationsGetToken = lstlocation[0];
                        InstallationsGetTokenPath = lstlocation[0].Substring(lstlocation[0].IndexOf("/v3"));
                        onSuccess();
                    }
                }
                else
                {
                    onFail();
                }
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, InstallationsPath);
                onFail();
            }
        }

        public IEnumerator SigninDevice(string email, string password, Action onSuccess,
            Action<string, Exception> onFail)
        {
            FibrumAPIDeviceInfo.SigninData signin = new FibrumAPIDeviceInfo.SigninData
            {
                email = email,
                password = password,
                deviceToken = DeviceToken
            };
            string jsonStr = JsonUtility.ToJson(signin);
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + SignupSilentPath), HTTPMethods.Post);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(jsonStr, "POST", SignupSilentPath, string.Empty));
            request.RawData = Encoding.UTF8.GetBytes(jsonStr);
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {
                    CurrentDeviceStatus = "confirmed";
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, SignupSilentPath);

                    HttpRequestChain chain = gameObject.AddComponent<HttpRequestChain>();

                    chain.Source = this;
                    chain.RequestChain = new List<ChainItem>();

                    chain.RequestChain.Add(new ChainItem(UserInfoRequest, () =>
                    {
                        Debug.Log("UserInfoRequest: Success");
                    },
                        () =>
                        {
                            Debug.Log("UserInfoRequest: Failed");
                            OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                        }));

                    chain.RequestChain.Add(new ChainItem(GetPremiumCodeInfo, () =>
                    {
                        Debug.Log("GetPremiumCodeInfo: Success");
                    },
                   () =>
                   {
                       Debug.Log("GetPremiumCodeInfo: Failed");
                       OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                   }));


                    if (AppFreemiumType == FreemiumType.TriesCount || AppFreemiumType == FreemiumType.TrialLevel)
                    {
                        chain.RequestChain.Add(new ChainItem(GetAvailableAppProducts, () =>
                        {
                            Debug.Log("GetAvailableAppProducts: Success");
                        },
                            () =>
                            {
                                Debug.Log("GetAvailableAppProducts: Failed");
                                OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
                            }));
                    }

                    chain.RequestChain.Add(new ChainItem(GetUserPurchases, () =>
                    {
                        Debug.Log("GetUserPurchases: Success");
                    },
               () =>
               {
                   Debug.Log("GetUserPurchases: Failed");
                   OnBlockApplication(AppBlockEventArgs.BlockEventType.HTTPRequestFail);
               }));

                    chain.StartCoroutine(chain.StartChain(() =>
                    {
                        Debug.Log("SigninDevice: Success");
                        onSuccess();
                    }, () => { }));
                }
                else
                {
                    if (data["message"] != null)
                    {
                        string message = data["message"].ToString();
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, message);
                        onFail(message, request.Exception);
                    }
                }
            }
            else
            {
                onFail(string.Empty, request.Exception);
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, SignupSilentPath);
            }
        }

        public IEnumerator GetBannerRequest(int bannerPlaceID, Action<JsonData> onSuccess, Action<string> onFail)
        {
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + BannersPath + "/" + bannerPlaceID + "?language=" + Application.systemLanguage + "&installationGuid="+InstallationGuid), HTTPMethods.Get);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization",
                GetAuthHeaderSignature(string.Empty, "GET", BannersPath + "/" + bannerPlaceID, "language=" + Application.systemLanguage + "&installationGuid="+InstallationGuid));
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, BannersPath);
                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {
                    onSuccess(data);
                }
                else
                {
                    if (data["message"] != null)
                    {
                        string message = data["message"].ToString();
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, message);
                        onFail(message);
                    }
                }
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, BannersPath);
                onFail(request.Exception.Message);
            }
        }

        public IEnumerator GetLocalization(SystemLanguage language, Action<string> onSuccess, Action onFail)
        {
            string path = "/v3/translations/menu";
            var request = new HTTPRequest(new Uri(serverURL + path + "?language=" + language), HTTPMethods.Get);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization",
                GetAuthHeaderSignature(string.Empty, "GET", path, "language=" + language));
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, BannersPath);
                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {
                    onSuccess(request.Response.DataAsText);
                }
                else
                {
                    if (data["message"] != null)
                    {

                        onFail();
                    }
                }
            }
            else
            {
                if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, BannersPath);
                onFail();
            }
        }

        public IEnumerator BannerContentRequest(FibrumAdsManager.Banner banner, Action<HTTPRequest> onSuccess, Action<string> onFail)
        {
            var request = new HTTPRequest(new Uri(banner.ContentURI), HTTPMethods.Get);
            //request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                //if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, UserInfoPath);
                //request.Response.DataAsTexture2D
                onSuccess(request);
            }
            else
            {
                onFail(request.Exception.Message);
            }
        }

        private IEnumerator UserInfoRequest(Action onSuccess, Action onFail)
        {
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + UserInfoPath + "/" + UserGuid), HTTPMethods.Get);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization",
                GetAuthHeaderSignature(string.Empty, "GET", UserInfoPath + "/" + UserGuid, string.Empty));
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, UserInfoPath);

                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {
                    string status = data["data"]["statusName"].ToString();
                    if (!string.IsNullOrEmpty(status))
                    {
                        CurrentUserStatus = status;
                        switch (status)
                        {
                            case "not_confirmed":
                                if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "User is not confirmed");
                                break;
                            case "wait_confirm":
                                if (FibrumAPIDebug.DEBUG)
                                    FibrumAPIDebug.LogMessage(this, "User is waiting confirmation");
                                OnBlockApplication(AppBlockEventArgs.BlockEventType.WaitingUserConfirm);
                                break;
                            case "confirm":
                                CurrentDeviceStatus = "confirmed";
                                if (FibrumAPIDebug.DEBUG)
                                    FibrumAPIDebug.LogMessage(this, "User is confirmed, checking premium code");
                                break;
                        }
                    }
                    string percentFill = data["data"]["percentFillProfile"].ToString();

                    if (!string.IsNullOrEmpty(percentFill))
                    {
                        PercentFillProfile = int.Parse(percentFill);
                    }
                    string fname = string.Empty;
                    string lname = string.Empty;

                    if (!string.IsNullOrEmpty(data["data"]["profile"].ToString()))
                    {
                        if (data["data"]["profile"].Keys.Contains("firstname") && data["data"]["profile"]["firstname"] != null)
                        {
                            fname = data["data"]["profile"]["firstname"].ToString();
                        }

                        if (data["data"]["profile"].Keys.Contains("lastname") && data["data"]["profile"]["lastname"] != null)
                        {
                            lname = data["data"]["profile"]["lastname"].ToString();
                        }
                    }

                    UserName = string.Format("{0} {1}", fname, lname);
                    onSuccess();
                }
                else
                {
                    if (data["message"] != null)
                    {
                        string message = data["message"].ToString();
                        if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, message);
                        onFail();
                    }
                }
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, UserInfoPath);
                onFail();
            }
        }

        public IEnumerator SendAndroidPurchase(string receipt, Action onSuccess, Action onFail)
        {
            FibrumAPIPayments.AndroidPurchase purchase = new FibrumAPIPayments.AndroidPurchase
            {
                userGuid = UserGuid,
                receipt = Convert.ToBase64String(Encoding.UTF8.GetBytes(receipt))
            };
            string jsonStr = JsonUtility.ToJson(purchase);
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + ReceiptsGooglePath), HTTPMethods.Post);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(jsonStr, "POST", ReceiptsGooglePath, string.Empty));
            request.RawData = Encoding.UTF8.GetBytes(jsonStr);
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, ReceiptsGooglePath);
                if (request.Response.StatusCode == 201)
                {
                    if(AppFreemiumType==FreemiumType.TrialLevel || AppFreemiumType==FreemiumType.TriesCount) SetPurchaseStatus(true);
                    onSuccess();
                }
                else
                {
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Fail sending Android purchase");
                    onFail();
                }
            }
            else
            {
                if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, ReceiptsGooglePath);
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, ReceiptsGooglePath);
                onFail();
            }
        }

        public IEnumerator SendIOSPurchase(string receipt, Action onSuccess, Action onFail)
        {
            FibrumAPIPayments.IOSPurchase purchase = new FibrumAPIPayments.IOSPurchase();
            purchase.userGuid = UserGuid;
            purchase.receipt = receipt;
            string jsonStr = JsonUtility.ToJson(purchase);
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + ReceiptsApplePath), HTTPMethods.Post);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(jsonStr, "POST", ReceiptsApplePath, string.Empty));
            request.RawData = Encoding.UTF8.GetBytes(jsonStr);
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, ReceiptsApplePath);
                if (request.Response.StatusCode == 201)
                {
                    SetPurchaseStatus(true);
                    onSuccess();
                }
                else
                {
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Fail sending iOS purchase");
                    onFail();
                }
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, ReceiptsApplePath);
                onFail();
            }
        }

        private IEnumerator GetUserPurchases(Action onSuccess, Action onFail)
        {
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + AppPaymentsPath + "/" + InstallationGuid), HTTPMethods.Get);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization",
                GetAuthHeaderSignature(string.Empty, "GET", AppPaymentsPath + "/" + InstallationGuid,
                    string.Empty));
            request.Send();
            yield return StartCoroutine(request);
            bool isPurchased = false;
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, AppPaymentsPath);
                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {
                    if (AppFreemiumType == FreemiumType.TriesCount || AppFreemiumType == FreemiumType.TrialLevel)
                    {
                        for (int i = 0; i < data["data"].Count; i++)
                        {

#if UNITY_IOS
                            string inAppid = data ["data"] [i] ["product_id"].ToString ();

                            //Debug.Log("ID --- "+inAppid+" ;   "+IOSNativeSettings.Instance.InAppProducts.Find(t=>t.Id ==inAppid).Id);
                            if (IOSNativeSettings.Instance.InAppProducts.Find(t=>t.Id ==inAppid)==null) 
                            {
                                var t = new IOSProductTemplate ();
                                t.Id = inAppid;
                                IOSNativeSettings.Instance.InAppProducts.Add (t);

                                var t2 = new UM_InAppProduct ();
                                t2.id = inAppid;
                                t2.SetTemplate (t);
                                UltimateMobileSettings.Instance.AddProduct (t2);
                            }
#endif

                            if (data["data"][i]["product_id"].ToString() == FullGameInApp)
                            {
                                if ((bool) data["data"][i]["paid"])
                                {
                                    isPurchased = true;
                                    SetPurchaseStatus(isPurchased);
                                    if (FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "App status: purchased");
                                }
                            }
                        }
                    }
                    InAppProducts = JsonMapper.ToJson(data["data"]);
                    InAppProductIDs = new List<string>();
                    for (int i = 0; i < data["data"].Count; i++)
                    {
                        InAppProductIDs.Add(data["data"][i]["product_id"].ToString());
                    }
                    onSuccess();
                }
                else
                {
                    if (data["message"] != null)
                    {
                        string message = data["message"].ToString();
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, message);
                        onFail();
                    }
                }
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, AppPaymentsPath);
                onFail();
            }
            OnUserPurchasesProcessed(isPurchased);
        }

        private IEnumerator GetPremiumCodeInfo(Action onSuccess, Action onFail)
        {
            currentTimestamp = GetTimestamp();
            string path = string.Format("/v3/premiums/users/{0}/installations/{1}", UserGuid, InstallationGuid);
            var request = new HTTPRequest(new Uri(serverURL + path), HTTPMethods.Get);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(string.Empty, "GET", path, string.Empty));
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, path);
                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {
                    DateTime date = DateTime.Parse(data["data"]["expirationDate"].ToString());
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Premium code expiry date:" + date.ToUniversalTime());
                    PremiumCodeExpiryDate = date.ToUniversalTime().ToString();
                    SetPCodeValidationTimestamp();
                }
                else
                {
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Premium code not found");
                    PremiumCodeExpiryDate = "";
                    if (data["message"] != null)
                    {
                        string message = data["message"].ToString();
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, message);
                    }
                }
                onSuccess();
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, AppPaymentsPath);
                onFail();
            }
        }

        private IEnumerator GetAvailableAppProducts(Action onSuccess, Action onFail)
        {
            currentTimestamp = GetTimestamp();
            string path = string.Format("/v3/payments/products/users/{0}/installations/{1}", UserGuid, InstallationGuid);
            var request = new HTTPRequest(new Uri(serverURL + path), HTTPMethods.Get);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(string.Empty, "GET", path, string.Empty));
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, path);
                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {
                    FullGameInApp = data["data"]["product_id"].ToString();
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Full game inapp:" + FullGameInApp);
                    DiscountPercent = int.Parse(data["data"]["discount"].ToString());
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Discount percent:" + DiscountPercent);
                    onSuccess();
                }
                else
                {
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "Inapp not found");
                    if (data["message"] != null)
                    {
                        string message = data["message"].ToString();
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, message);
                        onFail();
                    }
                }
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, AppPaymentsPath);
                onFail();
            }
        }

        private IEnumerator AppInfoRequest(Action onSuccess, Action onFail)
        {
            currentTimestamp = GetTimestamp();
            var request = new HTTPRequest(new Uri(serverURL + AppInfoPath + "/" + AppGuid), HTTPMethods.Get);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(string.Empty, "GET", AppInfoPath + "/" + AppGuid, string.Empty));
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, UserInfoPath);
                JsonData data = JsonMapper.ToObject(request.Response.DataAsText);
                if (data["status"].ToString() == "success")
                {

                    Debug.Log(data["data"]["trials"].ToString());
                    if (data["data"]["trials"] != null)
                    {
                        availableAppTrials = new Dictionary<string, int>();
                        for (int i = 0; i < data["data"]["trials"].Count; i++)
                        {
                            Debug.Log(data["data"]["trials"][i]["name"].ToString());
                            availableAppTrials.Add(data["data"]["trials"][i]["name"].ToString(), int.Parse(data["data"]["trials"][i]["count"].ToString()));
                        }
                    }
                    else
                    {
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "No trial tries data taken");
                    }
                    onSuccess();
                }
                else
                {
                    if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, "No trial tries data taken");
                    if (data["message"] != null)
                    {
                        string message = data["message"].ToString();
                        if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.LogMessage(this, message);
                        onFail();
                    }
                }
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, UserInfoPath);
                onFail();
            }
        }

        private IEnumerator RegisterUser()
        {
            Debug.Log("RegisterUser");
            string jsonStr = @"{""email"" : ""a.lukanov@fibrum.com""}";
            currentTimestamp = GetTimestamp();
            string path = "/v3/users/signup";
            var request = new HTTPRequest(new Uri(serverURL + path), HTTPMethods.Post);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(jsonStr, "POST", path, string.Empty));
            request.RawData = Encoding.UTF8.GetBytes(jsonStr);
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, path);
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, AppPaymentsPath);
            }
        }

        private IEnumerator RegisterUserConfirm()
        {
            Debug.Log("RegisterUserConfirm");
            string jsonStr =
                @"{""email"" : ""a.lukanov@fibrum.com"", ""code"" : ""4f7c63a9-5266-4d9c-afef-82d4d013a25b""}";
            currentTimestamp = GetTimestamp();
            string path = "/v3/users/signup/confirm";
            var request = new HTTPRequest(new Uri(serverURL + path), HTTPMethods.Post);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(jsonStr, "POST", path, string.Empty));
            request.RawData = Encoding.UTF8.GetBytes(jsonStr);
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, path);
                if (request.Response.Headers.ContainsKey("location"))
                {
                    List<string> lstlocation = request.Response.Headers["location"];
                    Debug.Log(lstlocation[0]);
                }
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, AppPaymentsPath);
            }
        }

        private IEnumerator UserSetPassword()
        {
            Debug.Log("UserSetPassword");
            string jsonStr = @"{""password"" : ""qwerty""}";
            string user = "b2b347ed-c1e6-4eac-97ab-374fb84fbc3c";
            currentTimestamp = GetTimestamp();
            string path = string.Format("/v3/users/{0}/setPassword", user);
            var request = new HTTPRequest(new Uri(serverURL + path), HTTPMethods.Post);
            request.SetHeader("Content-Type", "application/json; charset=UTF-8");
            request.AddHeader("Authorization", GetAuthHeaderSignature(jsonStr, "POST", path, string.Empty));
            request.RawData = Encoding.UTF8.GetBytes(jsonStr);
            request.Send();
            yield return StartCoroutine(request);
            if (request.Exception == null)
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPResponse(request.Response, path);
            }
            else
            {
                if(FibrumAPIDebug.DEBUG) FibrumAPIDebug.DebugHTTPException(request.Exception, AppPaymentsPath);
            }
        }

        void OnGUI()
        {
//            if (GUI.Button(new Rect(Screen.width - 400f, Screen.height - 50f, 200f, 50f), "WritePurchases"))
//            {
//                Debug.Log(InAppProducts);
//                WriteInAppProductsData("inapp1_full", true);
//                Debug.Log(InAppProducts);
//            }

            //if (GUI.Button(new Rect(Screen.width - 400f, Screen.height - 50f, 200f, 50f), "Fake tries count"))
            //{
            //    Debug.Log(string.Format("Fake {0} value--", "default"));
            //    SetTrialsData("default", --Trials["default"]);
            //    Debug.Log(string.Format("default:{0}", Trials["default"]));
            //}

            //if (GUI.Button(new Rect(Screen.width - 600f, Screen.height - 50f, 200f, 50f), "TrialLevelEnd"))
            //{
            //    SetTrialLevelEndReached();
            //    Debug.Log("Fake triallevel end reached");
            //}

            //if (GUI.Button(new Rect(Screen.width - 800f, Screen.height - 50f, 200f, 50f), "Clear cache"))
            //{
            //    PlayerPrefs.DeleteAll();
            //}
            //if (GUI.Button(new Rect(Screen.width - 1000f, Screen.height - 50f, 200f, 50f), "Fake android buy"))
            //{
            //    SetPurchaseStatus(true);
            //    FNUIUserPanel.Instance.OnBillingBuy();
            //}
        }
    }
}

