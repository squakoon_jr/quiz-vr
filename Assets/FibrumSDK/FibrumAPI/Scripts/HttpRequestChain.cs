﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HttpRequestChain : MonoBehaviour
{
    public List<ChainItem> RequestChain;
    public MonoBehaviour Source;
    private int current;

	public IEnumerator StartChain(Action onDone, Action onFail)
    {
        while (current < RequestChain.Count)
        {
				yield return Source.StartCoroutine(RequestChain[current].func(RequestChain[current].onSuccess, RequestChain[current].onFail));
                current++;
        }
        StopChain();
	    if (onDone != null) onDone();
    }

    private void StopChain()
    {
        Destroy(this);
    }
}
