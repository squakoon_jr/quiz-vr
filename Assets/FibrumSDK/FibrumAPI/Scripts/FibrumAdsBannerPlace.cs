﻿using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.FibrumAPI.Scripts
{
    public class FibrumAdsBannerPlace : MonoBehaviour
    {

        // Use this for initialization
        public int BannerPlaceID;
        public bool RequestOnStart;
        public RawImage BannerPlace;
        private FibrumAdsManager.Banner banner;
        public int Duration;
        private float lastEventTime;

        public enum InitializeCondition
        {
            OnStart,
            OnClick
        }

        public InitializeCondition BannerInitializeCondition;

        void Start ()
        {
            //FibrumAPI.InitializedEvent += OnAPIInitialized;
            if (!FibrumAPI.NetworkAvailable || FibrumAPI.Instance.IsPurchased || FibrumAPI.Instance.PremiumCodeActive)
            {
                FibrumAdsManager.Instance.LoadNextLevel();
                return;
            }

            BannerPlace.enabled = false;
            if (BannerInitializeCondition == InitializeCondition.OnStart)
            {
                FibrumAdsManager.Instance.BannerLoadedEvent += OnBannerLoaded;
                GetBannerByID(BannerPlaceID);
            }


            //BannerPlace.raycastTarget = (BannerInitializeCondition != InitializeCondition.OnStart);
        }

        private void SetAspect(float width, float height)
        {
            gameObject.GetComponent<AspectRatioFitter>().aspectRatio = width/height;
        }

        //public void OnAPIInitialized(object sender, EventArgs e)
        //{
        //    FibrumAPI.InitializedEvent -= OnAPIInitialized;

        //    if (BannerInitializeCondition == InitializeCondition.OnStart)
        //    {
        //        FibrumAdsManager.Instance.BannerLoadedEvent += OnBannerLoaded;
        //        RequestBanner(BannerPlaceID);
        //    }
        //}

        public void GetBannerByID(int bannerPlaceID)
        {
            FibrumAdsManager.Instance.RequestBanner(bannerPlaceID);
        }

        public void OnBannerLoaded(object sender, FibrumAdsManager.BannerLoadEventArgs args)
        {
            FibrumAdsManager.Instance.BannerLoadedEvent -= OnBannerLoaded;
            banner = args.LoadedBanner;
            StartCoroutine(ShowBanner());
        }

        public IEnumerator ShowBanner()
        {
            string[] eventParams = new string[10];
			eventParams[0] = BannerPlaceID.ToString();
			eventParams [1] = FibrumAdsManager.Instance.CurrentBanner.BannerID.ToString ();
			eventParams[2] = (Time.time-FibrumAdsManager.Instance.CurrentBanner.RequestData.downloadedTime).ToString();
			eventParams [3] = FibrumAdsManager.Instance.CurrentBanner.BannerSessionID.ToString ();

			FibrumAPIEvents.Instance.SendCustomEvent((int)FibrumAdsManager.CrossPromoEvent.Shown, eventParams, banner.BannerSessionID);

            switch (banner.Type)
            {
                case FibrumAdsManager.BannerType.FullscreenImage:
                    BannerPlace.texture = ((FibrumAdsManager.FullscreenImageBanner) banner).BannerImage;
                    SetAspect(BannerPlace.texture.width, BannerPlace.texture.height);
                    break;
            }
            BannerPlace.enabled = true;
            yield return new WaitForSeconds(Duration);
            gameObject.SetActive(false);

            string[] eventParams1 = new string[10];
			eventParams1[0] = BannerPlaceID.ToString();
			eventParams [1] = FibrumAdsManager.Instance.CurrentBanner.BannerID.ToString ();
			eventParams[2] = (Time.time - FibrumAdsManager.Instance.CurrentBanner.RequestData.bannerShownTime).ToString();
			eventParams [3] = FibrumAdsManager.Instance.CurrentBanner.BannerSessionID.ToString ();
			FibrumAPIEvents.Instance.SendCustomEvent((int)FibrumAdsManager.CrossPromoEvent.Timeout, eventParams1, banner.BannerSessionID);
            FibrumAdsManager.Instance.LoadNextLevel();
        }

        public void OnBannerPlaceClick()
        {
            if (banner != null)
            {
                string[] eventParams = new string[10];
				eventParams[0] = BannerPlaceID.ToString();
				eventParams [1] = FibrumAdsManager.Instance.CurrentBanner.BannerID.ToString ();
				eventParams[2] = (Time.time - FibrumAdsManager.Instance.CurrentBanner.RequestData.bannerShownTime).ToString();
				eventParams [3] = FibrumAdsManager.Instance.CurrentBanner.BannerSessionID.ToString ();
				FibrumAPIEvents.Instance.SendCustomEvent((int)FibrumAdsManager.CrossPromoEvent.Clicked, eventParams, banner.BannerSessionID);

                //gameObject.SetActive(false);
                Application.OpenURL(banner.LinkURI);
                Debug.Log("Banner clicked!");
                FibrumAdsManager.Instance.LoadNextLevel();
            }
        }

        public void OnCloseButtonClick()
        {
            FibrumAdsManager.Instance.LoadNextLevel();
            string[] eventParams = new string[10];
			eventParams[0] = BannerPlaceID.ToString();
			eventParams [1] = FibrumAdsManager.Instance.CurrentBanner.BannerID.ToString ();
			eventParams[2] = (Time.time - FibrumAdsManager.Instance.CurrentBanner.RequestData.bannerShownTime).ToString();
			eventParams [3] = FibrumAdsManager.Instance.CurrentBanner.BannerSessionID.ToString ();
			FibrumAPIEvents.Instance.SendCustomEvent((int)FibrumAdsManager.CrossPromoEvent.Closed, eventParams, banner.BannerSessionID);

            //if (banner != null)
            //{
            //    //gameObject.SetActive(false);
            //    //Application.OpenURL(banner.LinkURI);
            //    //Debug.Log("Banner clicked!");
            //    //Application.LoadLevel(Application.loadedLevel+1);
            //}
        }




    }
}
