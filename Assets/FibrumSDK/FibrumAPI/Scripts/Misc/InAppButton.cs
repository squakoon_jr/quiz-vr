﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.FibrumAPI.Scripts;

public class InAppButton : MonoBehaviour
{

    public string InappID;
    private bool isClicked;
    private bool isPurchased;

    private void CheckPurchase()
    {
        isPurchased = FibrumAPI.Instance.GetInAppProductStatus(InappID);
        gameObject.GetComponent<Image>().color = isPurchased ? Color.green : Color.white;
    }

    void Start()
    {
        CheckPurchase();
    }

    public void OnButtonClick()
    {
        if(isClicked) return;
        
        Debug.Log("Purchasing inapp: " + InappID);
        FibrumAPIBilling.Instance.PurchaseSelected(InappID);
        isClicked = true;
    }
	
}
