﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Assets.FibrumAPI.Scripts
{
    public class FNUIApiGrab : MonoBehaviour
    {
        public enum EGrabType
        {
            UserName,
            DiscountPercent,
            FullGamePrice,
            SmallPrice,
            DiscountBadge
        }

        public EGrabType grabType;

        private Text txt;
        private Image img;

        void Awake()
        {
            if (grabType == EGrabType.FullGamePrice && grabType == EGrabType.DiscountBadge)
            {
                FibrumAPIBilling.FullGamePurchaseTargetUpdated += OnFullGamePurchaseTargetUpdated;
            }
        }

        private void OnFullGamePurchaseTargetUpdated(object sender, System.EventArgs e)
        {
            if (!txt)
                txt = GetComponent<Text>();

            Debug.Log("Component Text is null:" + (txt==null));
            Debug.Log("PurchaseTarget is null:" + (FibrumAPIBilling.Instance.PurchaseTarget == null));
            txt.text = FibrumAPIBilling.Instance.PurchaseTarget.LocalizedPrice;
            //OnEnable();
            Debug.Log("FNUIApiGrab Update");
        }

        void OnDestroy()
        {
            FibrumAPIBilling.FullGamePurchaseTargetUpdated -= OnFullGamePurchaseTargetUpdated;
        }

        private void OnEnable()
        {
            if (!txt)
                txt = GetComponent<Text>();

            if (!img)
                img = GetComponentInChildren<Image>();

            switch (grabType)
            {
                case EGrabType.UserName:
                    txt.text = FibrumAPI.Instance.UserName;
                    break;

                case EGrabType.DiscountPercent:
                    txt.text = FibrumAPI.Instance.DiscountPercent.ToString();
                    break;

                case EGrabType.FullGamePrice:
                    string t = FibrumAPIBilling.Instance.PurchaseTarget.LocalizedPrice;
                    if (t == "" || t.Length < 3)
                        t = "0 $";
                    txt.text = t;
                    break;

                case EGrabType.SmallPrice:
                    string pr = FibrumAPIBilling.Instance.PurchaseTarget.LocalizedPrice;
                    string sign = " $";
                    string[] lines;
                    if (pr.Contains(" "))
                    {
                        lines = pr.Split(new char[] {' '}, System.StringSplitOptions.RemoveEmptyEntries);
                        pr = lines[0];
                        if (lines.Length > 1)
                        {
                            sign = " " + lines[lines.Length - 1];
                        }
                    }

                    float p = 0f;
                    float.TryParse(pr, out p);

                    if (p != 0f)
                    {
                        p = p/(float) (100 - FibrumAPI.Instance.DiscountPercent)*100f;
                        txt.text = p + sign;
                        img.enabled = true;
                    }
                    else
                    {
                        txt.text = "";
                        img.enabled = false;
                    }
                    break;

                case EGrabType.DiscountBadge:
                    int d = FibrumAPI.Instance.DiscountPercent;
                    if (d > 0)
                    {
                        img.enabled = true;
                        if (txt) txt.text = d + "%";
                    }
                    else
                    {
                        img.enabled = false;
                        if (txt) txt.text = "";
                    }
                    break;
            }
        }
    }
}