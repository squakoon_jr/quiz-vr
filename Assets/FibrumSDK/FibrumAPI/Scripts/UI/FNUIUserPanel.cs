﻿using System;
using System.Linq;
using System.Text;
using Assets.FibrumAPI.Scripts.FibrumLocalization;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.FibrumAPI.Scripts.UI
{
    public class FNUIUserPanel : MonoBehaviour
    {
        public string FibrumNetworkRegisterURL;
        //public string FibrumNetworkSupportURL;
        public string FibrumNetworkAppURL;
        public string FibrumNetworkAppBundle;
        public GameObject LoginButton;
        public GameObject BuyButton;
        public GameObject RegisterButton;
        public GameObject RestoreButtonLink;
        public GameObject SupportButtonLink;
        public GameObject LoginButtonLink;

        public GameObject UserInfo;
        public GameObject PurchaseInfo;
        public GameObject TrialInfo;
        public GameObject PremiumCodeInfo;
        public GameObject LoginPanel;

        public GameObject LoginPanelStart;
        public GameObject LoginPanelWait;
        public GameObject LoginPanelError;

        public GameObject SettingsPanel;
        public GameObject FibrumStoreInstallPanel;

        public GameObject BackButton;

        public GameObject TrialEndText;
        public Text TriesLeftText;
        public Text TriesDataText;

        public Text TimeLeftDataText;
        public Text TimeMeasureText;

        public GameObject LoginPanelErrorHeaderText;
        public GameObject LoginPanelErrorMessageText;

        public GameObject LoginInputError;
        public InputField EmailInputField;
        public InputField PasswordInputField;

        public GameObject StatusText;

        private bool loginProcessing;
        private bool fibrumStoreOfferOnRegisterClick;

        public static FNUIUserPanel Instance;

		enum ButtonName
		{
			Buy,
			RestorePurchase,
			Login,
			LearnMore,
			Back,
			Register
		};

        void Awake()
        {
            Debug.Log("FNUIUserPanel Awake");
            if (Instance == null) Instance = this;
            else if (Instance != this) Destroy(gameObject.GetComponent(Instance.GetType()));
            DontDestroyOnLoad(gameObject);
            FibrumAPI.Instance.CheckIsApplicationAvailable();
            //FibrumAPIBilling.Instance.BillingReady += OnFreemiumBillingReady;
        }

        public enum FNUIState
        {
            FNTrialOptions,
            FNLogin,
            FNWelcome,
            FNPurchased,
            FNError
        }

        public FNUIState CurrentFNUIState;

        private string GetTextLocalizationKey(int hoursLeft, int daysLeft)
        {
            if (daysLeft <= 0)
            {
                if (hoursLeft == 1)
                {
                    return "1_hour_left";
                }
                if (hoursLeft > 1 && hoursLeft < 5)
                {
                    return "2_4_hours_left";
                }
                if (hoursLeft > 4)
                {
                    return "n_hours_left";
                }
            }
            if (daysLeft == 1)
            {
                return "1_day_left";
            }
            if (daysLeft >= 2 && daysLeft<=4)
            {
                return "2_4_days_left";
            }
            if (daysLeft > 4)
            {
                return "n_days_left";
            }
            return string.Empty;
        }

        public void SetState()
        {
            //CurrentFNUIState = state;

            bool appActive = (FibrumAPI.Instance.IsPurchased || FibrumAPI.Instance.TrialActive || FibrumAPI.Instance.PremiumCodeActive || FibrumAPI.Instance.AppFreemiumType == FibrumAPI.FreemiumType.CustomInApps);

            BackButton.SetActive(appActive);

            TrialInfo.SetActive(!FibrumAPI.Instance.IsPurchased && !FibrumAPI.Instance.PremiumCodeActive);
            UserInfo.SetActive(FibrumAPI.Instance.IsDeviceConfirmed);
            PremiumCodeInfo.SetActive(!FibrumAPI.Instance.IsPurchased && FibrumAPI.Instance.PremiumCodeActive);
            PurchaseInfo.SetActive(FibrumAPI.Instance.IsPurchased);

            if (FibrumAPI.Instance.AppFreemiumType != FibrumAPI.FreemiumType.CustomInApps)
            {
                BuyButton.SetActive(!FibrumAPI.Instance.IsPurchased);
            }
            else
            {
                BuyButton.SetActive(false);
            }

            LoginButton.SetActive(appActive && !FibrumAPI.Instance.IsDeviceConfirmed);
            RestoreButtonLink.SetActive(!FibrumAPI.Instance.IsPurchased);
            RegisterButton.SetActive(!appActive && !FibrumAPI.Instance.IsDeviceConfirmed);
            LoginButtonLink.SetActive(!appActive && !FibrumAPI.Instance.IsDeviceConfirmed);
            TrialEndText.SetActive(!appActive);
            StatusText.SetActive(appActive);
            TriesLeftText.gameObject.SetActive(FibrumAPI.Instance.TrialActive);
            TriesDataText.gameObject.SetActive(FibrumAPI.Instance.TrialActive);
            SettingsPanel.SetActive(appActive);
            SupportButtonLink.SetActive(true);

            if (appActive)
            {
                LoginButton.transform.localPosition = new Vector3(-480f, 145f, 0f);
                BuyButton.transform.localPosition = new Vector3(-480f, -90f, 0f);
                RestoreButtonLink.transform.localPosition = new Vector3(-480f, -260f, 0f);
                SupportButtonLink.transform.localPosition = new Vector3(-480f, -360f, 0f);
                TrialInfo.transform.localPosition = new Vector3(-480f, 315f, 0f);
                PremiumCodeInfo.transform.localPosition = new Vector3(-480f, 315f, 0f);
                UserInfo.transform.localPosition = new Vector3(-660f, 140f, 0f);
                if (FibrumAPI.Instance.PremiumCodeActive)
                {
                    int hoursLeft = FibrumAPI.Instance.PremiumCodeHoursLeft;
                    int daysLeft = hoursLeft/24;
                    TimeMeasureText.gameObject.GetComponent<TextLocalization>()
                        .SetKey(GetTextLocalizationKey(hoursLeft, daysLeft));
                    TimeLeftDataText.text = daysLeft > 0 ? daysLeft.ToString() : hoursLeft.ToString();
                }

                if (FibrumAPI.Instance.IsPurchased)
                {
                    StatusText.GetComponent<TextLocalization>().SetKey("fn_fullaccess");
                }
                else
                {
                    if (FibrumAPI.Instance.PremiumCodeActive)
                    {
                        StatusText.GetComponent<TextLocalization>().SetKey("fn_premiumcode");
                    }
                    else
                    {
                        if (FibrumAPI.Instance.TrialActive)
                        {
                            StatusText.GetComponent<TextLocalization>().SetKey("fn_trial");
                        }
                    }
                }

                if (FibrumAPI.Instance.AppFreemiumType == FibrumAPI.FreemiumType.TriesCount)
                {
                    if (FibrumAPI.Instance.Trials.Count == 1)
                    {
                        TriesLeftText.gameObject.SetActive(true);
                        TriesDataText.gameObject.SetActive(true);
                        TriesLeftText.GetComponent<TextLocalization>().SetKey("fn_trialoptions_trialmode_triesleft");
                        TriesDataText.text = FibrumAPI.Instance.Trials.ElementAt(0).Value.ToString();
                    }
                    else
                    {
                        TriesLeftText.gameObject.SetActive(false);
                        TriesDataText.gameObject.SetActive(false);
                    }
                }
                else
                {
                    TriesLeftText.gameObject.SetActive(false);
                    TriesDataText.gameObject.SetActive(false);
                }
            }
            else
            {
                TriesLeftText.gameObject.SetActive(false);
                TriesDataText.gameObject.SetActive(false);
                UserInfo.transform.localPosition = new Vector3(-240f, 120f, 0f);
                if (FibrumAPI.Instance.IsDeviceConfirmed)
                {
                    RestoreButtonLink.transform.localPosition = new Vector3(0f, -215f, 0f);
                    BuyButton.transform.localPosition = new Vector3(0f, -70f, 0f);
                }
                else
                {
                    RestoreButtonLink.transform.localPosition = new Vector3(300f, -215f, 0f);
                    BuyButton.transform.localPosition = new Vector3(300f, -45f, 0f);
                }
                LoginButton.transform.localPosition = new Vector3(-480f, 145f, 0f);
                RegisterButton.transform.localPosition = new Vector3(-300f, -45f, 0f);
                LoginButtonLink.transform.localPosition = new Vector3(-300f, -215f, 0f);
                SupportButtonLink.transform.localPosition = new Vector3(0f, -360f, 0f);
                TrialInfo.transform.localPosition = new Vector3(0f, 270f, 0f);
                PremiumCodeInfo.transform.localPosition = new Vector3(0f, 270f, 0f);
            }
        }

        //public void OnRetryRequestButtonClick()
        //{
        //    FNUIBase.Instance.OnCloseUpwardButtonclick();
        //    FibrumAPI.Instance.Init();
        //}

        //public void OnRetryConnectionButtonClick()
        //{
        //    FNUIBase.Instance.OnCloseUpwardButtonclick();
        //    FibrumAPI.Instance.Init();
        //}

        public void OnBackButtonClick()
        {
            bool appActive = (FibrumAPI.Instance.IsPurchased || FibrumAPI.Instance.TrialActive ||
                              FibrumAPI.Instance.PremiumCodeActive);

            if (LoginPanel.activeSelf)
            {
                if (LoginPanelStart.activeSelf)
                {
                    LoginPanel.SetActive(false);
                    return;
                }
                if (LoginPanelError.activeSelf)
                {
                    LoginPanelStart.SetActive(true);
                    LoginPanelWait.SetActive(false);
                    LoginPanelError.SetActive(false);
                    return;
                }

				OnButtonClick (ButtonName.Back);
            }
            else
            {
                if (appActive)
                {
                    FNUIBase.Instance.CloseUIPanel();
                    return;
                }
            }
        }

        private void TryOpenApp()
        {
            if (Application.platform != RuntimePlatform.Android)
            {
                Application.OpenURL(FibrumNetworkAppURL);
                return;
            }

            bool fail = false;
            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
            AndroidJavaObject launchIntent = null;
            try
            {
                launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", FibrumNetworkAppBundle);
            }
            catch (System.Exception e)
            {
                fail = true;
            }

            if (fail)
            {
                Application.OpenURL(FibrumNetworkAppURL);
            }
            else
            {
                ca.Call("startActivity", launchIntent);
            }

            up.Dispose();
            ca.Dispose();
            packageManager.Dispose();
            launchIntent.Dispose();

        }

        public void ShowFibrumStorePanelOffer()
        {
            FibrumStoreInstallPanel.SetActive(true);
        }

        public void OnFibrumStoreContinueButtonClick()
        {
            FibrumStoreInstallPanel.SetActive(false);
            TryOpenApp();
        }

		public void OnFinishTextInput(string eventMessage)
		{
			string[] eventParams = new string[10];

			eventParams [0] = "Form";
			eventParams [1] = "Enter";
			eventParams [2] = eventMessage;

			FibrumAPIEvents.Instance.SendCustomEvent (FibrumAPIEvents.EventType.Registration, eventParams, string.Empty);
		}

        public void OnFibrumStoreCancelButtonClick()
        {
            FibrumStoreInstallPanel.SetActive(false);
            if (fibrumStoreOfferOnRegisterClick)
            {
                fibrumStoreOfferOnRegisterClick = false;
                Application.OpenURL(FibrumNetworkRegisterURL);
            }
        }

		void OnButtonClick(ButtonName btn)
		{
			string[] eventParams = new string[10];

			eventParams [0] = "Button";
			eventParams [1] = "Click";
			eventParams [2] = btn.ToString ();

			FibrumAPIEvents.Instance.SendCustomEvent (FibrumAPIEvents.EventType.Registration, eventParams, string.Empty);
		}

        public void OnBuyButtonClick()
        {
            if(!FibrumAPIBilling.Instance.IsInitialized) return;
            FibrumAPIBilling.Instance.FullGamePurchase();
            Debug.Log("Purchasing");

			OnButtonClick (ButtonName.Buy);
        }

        public void OnRestoreButtonClick()
        {
            if (!FibrumAPIBilling.Instance.IsInitialized) return;
            FibrumAPIBilling.Instance.Restore();
            Debug.Log("Restoring Purchase");

			OnButtonClick (ButtonName.RestorePurchase);
        }

        public void OnBillingBuy()
        {
            UpdatePanel();
        }

        public void OnBillingRestore()
        {
            UpdatePanel();
        }

        public void OnLoginButtonClick()
        {
            LoginInputError.SetActive(false);
            LoginPanel.SetActive(true);
            LoginPanelStart.SetActive(true);
            LoginPanelWait.SetActive(false);
            LoginPanelError.SetActive(false);

			OnButtonClick (ButtonName.Login);
        }

        public void OnTextInput()
        {
            if (LoginInputError.activeSelf)
            {
                LoginInputError.SetActive(false);
            }
        }

        public void OnLoginBackgroundClick()
        {
            if(!loginProcessing) LoginPanel.SetActive(false);
        }

        public void OnSendCredentialsButtonClick()
        {
            if (!string.IsNullOrEmpty(PasswordInputField.text) && PasswordInputField.text.Length > 5 &&
                !string.IsNullOrEmpty(EmailInputField.text) && Utils.Utils.IsValidEmail(EmailInputField.text))
            {
                Debug.Log("Valid email and password input");
                FibrumAPI.Instance.StartCoroutine(FibrumAPI.Instance.SigninDevice(EmailInputField.text, PasswordInputField.text, OnSigninDeviceSuccess, OnSigninDeviceFail));
                loginProcessing = true;
                LoginPanelStart.SetActive(false);
                LoginPanelWait.SetActive(true);
                LoginPanelError.SetActive(false);
            }
            else
            {
                LoginInputError.SetActive(true);
                Debug.Log("Email or password is empty or invalid");
            }
        }

        public void OnSigninDeviceSuccess()
        {
            LoginPanel.SetActive(false);
            loginProcessing = false;
            BackButton.SetActive(false);
            SetState();
            FibrumAPIBilling.Instance.UpdateFullGameInAppProduct();
        }

        public string GetSigninErrorLocalizationKey(string error)
        {
            switch (error)
            {
                case "InvalidJsonData":
                    return "fail_message_sys_InvalidJsonData";
                case "InvalidFormatEmail":
                    return "fail_message_sys_InvalidFormatEmail";
                case "InvalidFormatPassword":
                    return "fail_message_sys_InvalidFormatPassword";
                case "InvalidDeviceToken":
                    return "fail_message_sys_InvalidDeviceToken";
                case "UserOrPasswordNotFound":
                    return "fail_message_user_UserOrPasswordNotFound";
                case "UserNotConfirmed":
                    return "fail_message_user_UserNotConfirmed";
                case "DeviceNotFound":
                    return "fail_message_sys_DeviceNotFound";
                case "DatabaseError":
                    return "fail_message_sys_DatabaseError";
            }
            return "start_error_http";
        }

        public void OnSigninDeviceFail(string message, Exception ex)
        {
            loginProcessing = false;
            LoginPanelStart.SetActive(false);
            LoginPanelWait.SetActive(false);
            LoginPanelError.SetActive(true);

            if (ex == null)
                LoginPanelErrorMessageText.gameObject.GetComponent<TextLocalization>().SetKey(GetSigninErrorLocalizationKey(message));
            else LoginPanelErrorMessageText.gameObject.GetComponent<TextLocalization>().SetKey(ex.Message);
            //BackButton.SetActive(true);
        }

        public void OnRegisterLinkClick()
        {
            fibrumStoreOfferOnRegisterClick = true;
            if (!PlayerPrefs.HasKey("FibrumStoreOffered") || PlayerPrefs.GetInt("FibrumStoreOffered") == 1)
            {
                Debug.Log("FibrumStoreOffered write Prefs 2");
                PlayerPrefs.SetInt("FibrumStoreOffered", 2);
                //ShowFibrumStorePanelOffer();
            }
            else
            {
#if UNITY_IOS && USES_WEBVIEW
      
                WebViewController.Instance.Open("https://fibrum.net/users/signup");
#else
                Application.OpenURL(FibrumNetworkRegisterURL);
#endif
            }

			OnButtonClick (ButtonName.Register);
        }

        public void OnFibNetworkButtonClick()
        {
            if (!PlayerPrefs.HasKey("FibrumStoreOffered") || PlayerPrefs.GetInt("FibrumStoreOffered") == 1)
            {
                Debug.Log("FibrumStoreOffered write Prefs 2");
                PlayerPrefs.SetInt("FibrumStoreOffered", 2);
                //ShowFibrumStorePanelOffer();
            }
            else
            {
                TryOpenApp();
            }
        }


        public void OnSupportButtonLinkClick()
        {
#if UNITY_IOS
             var sb = new StringBuilder();
          string info = string.Format("Application: {0}\n" +
                                      "Device: {1}\n" +
                                      "Operating System: {2}\n",
              Application.productName, SystemInfo.deviceModel, SystemInfo.operatingSystem);
           sb.Append("mailto:support@fibrum.com?");
           sb.Append(("subject=Troubleshooting&body="));
           sb.Append(MyEscapeURL(info));
           sb.Append(MyEscapeURL("Please describe your issue here:"));

          Application.OpenURL(sb.ToString());
#else
            var sb = new StringBuilder();
            string info = string.Format("Application: {0}\n" +
                                        "Device: {1}\n" +
                                        "Operating System: {2}\n",
                Application.productName, SystemInfo.deviceModel, SystemInfo.operatingSystem);
            sb.Append("mailto:support@fibrum.com?subject=Troubleshooting&body=");
            sb.Append(info);
            sb.Append("Please%20describe%20your%20issue%20here:");
            Application.OpenURL(sb.ToString());
#endif
        }

        public static string MyEscapeURL(string url)
        {
            return WWW.EscapeURL(url).Replace("+", "%20");
        }

        public void UpdatePanel()
        {
            Debug.Log("Updating Club Panel");
            StartPanel();
        }

        public void StartPanel()
        {
            if (!PlayerPrefs.HasKey("FibrumStoreOffered"))
            {
                Debug.Log("FibrumStoreOffered write Prefs");
                PlayerPrefs.SetInt("FibrumStoreOffered", 1);
                //ShowFibrumStorePanelOffer();
            }

            SetState();
        }
    }
}
