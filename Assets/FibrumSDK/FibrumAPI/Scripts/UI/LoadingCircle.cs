﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.FibrumAPI.Scripts.UI
{
    public class LoadingCircle : MonoBehaviour {

        public Image CircleImage;

        void Start () {
        }
	
        void Update () {
            transform.Rotate(-Vector3.forward * Time.deltaTime * 200f, Space.Self);
            CircleImage.fillAmount = (Mathf.Sin(Time.time)/2f)+0.5f;
        }
    }
}
