﻿using Assets.FibrumAPI.Scripts.FibrumLocalization;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.FibrumAPI.Scripts.UI
{
    public class FibrumStartSceneUI : MonoBehaviour
    {

        public Image ProgressbarForeground;
        public RectTransform ProgressBar;
        public Text ErrorText;
        // Use this for initialization
        void Start ()
        {
            FibrumAPI.InitializationProgressEvent += OnInitializationProgressEvent;
            FibrumAPI.Instance.BlockApplication += OnBlockApplication;
        }

        private void OnBlockApplication(object sender, FibrumAPI.AppBlockEventArgs e)
        {
            FibrumAPI.Instance.BlockApplication -= OnBlockApplication;
            ErrorText.gameObject.SetActive(true);
            ProgressBar.gameObject.SetActive(false);
            switch (e.AppBlockType)
            {
                case FibrumAPI.AppBlockEventArgs.BlockEventType.HTTPRequestFail:
                    ErrorText.GetComponent<TextLocalization>().SetKey("start_error_http");
                    break;
                case FibrumAPI.AppBlockEventArgs.BlockEventType.NetworkUnavailable:
                    ErrorText.GetComponent<TextLocalization>().SetKey("start_error_no_network");
                    break;
                case FibrumAPI.AppBlockEventArgs.BlockEventType.WaitingUserConfirm:
                    ErrorText.GetComponent<TextLocalization>().SetKey("start_error_user_not_confirmed");
                    break;
                case FibrumAPI.AppBlockEventArgs.BlockEventType.TimeSyncFail:
                    ErrorText.GetComponent<TextLocalization>().SetKey("start_error_datetime_invalid");
                    break;
            }
        }

        public void OnInitializationProgressEvent(object sender, FibrumAPI.InitializationProgressEventArgs args)
        {
            if (Mathf.Approximately(args.Progress, 1f))
            {
                FibrumAPI.Instance.BlockApplication -= OnBlockApplication;
                FibrumAPI.InitializationProgressEvent -= OnInitializationProgressEvent;
            }
            //ProgressbarForeground.fillAmount = (args.Progress*ProgressBar.sizeDelta.x)/ProgressBar.sizeDelta.x;
            ProgressbarForeground.fillAmount = args.Progress;
        }
    
    }
}

