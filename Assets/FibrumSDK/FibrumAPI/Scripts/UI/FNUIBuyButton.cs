﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Assets.FibrumAPI.Scripts
{
    public class FNUIBuyButton : MonoBehaviour
    {
        public Text PriceText;
        public GameObject DiscountBadge;
        public Text DiscountValueText;
        public Text FullPriceValueText;

        private void Awake()
        {
            FibrumAPIBilling.FullGamePurchaseTargetUpdated += OnFullGamePurchaseTargetUpdated;
        }

        private void OnFullGamePurchaseTargetUpdated(object sender, System.EventArgs e)
        {
            Debug.Log("PurchaseTarget is null:" + (FibrumAPIBilling.Instance.PurchaseTarget == null));
            Debug.Log("FNUIBuyButton Update");
            UpdateValues();
        }

        void OnDestroy()
        {
            FibrumAPIBilling.FullGamePurchaseTargetUpdated -= OnFullGamePurchaseTargetUpdated;
        }

        private void UpdateValues()
        {
            PriceText.text = FibrumAPIBilling.Instance.PurchaseTarget.LocalizedPrice;

            Debug.Log(FibrumAPIBilling.Instance.PurchaseTarget.LocalizedPrice);
            Debug.Log(FibrumAPIBilling.Instance.PurchaseTarget.ActualPrice);
            Debug.Log(FibrumAPIBilling.Instance.PurchaseTarget.ActualPriceValue);

            if (FibrumAPI.Instance.DiscountPercent > 0)
            {
                DiscountBadge.SetActive(true);
                DiscountValueText.text = FibrumAPI.Instance.DiscountPercent + "%";
                FullPriceValueText.gameObject.SetActive(true);
                FullPriceValueText.text = (FibrumAPIBilling.Instance.PurchaseTarget.ActualPriceValue/(100f - FibrumAPI.Instance.DiscountPercent) * 100f).ToString("N2");
            }
            else
            {
                DiscountBadge.SetActive(false);
                FullPriceValueText.gameObject.SetActive(false);
                DiscountValueText.text = 0 + "%";
            }
        }

        private void OnEnable()
        {
            UpdateValues();
        }
    }
}