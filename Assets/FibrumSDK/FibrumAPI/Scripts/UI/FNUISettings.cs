﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.FibrumAPI.Scripts.UI
{
    public class FNUISettings : MonoBehaviour
    {
        [System.Serializable]
        public struct ToggleDisplayMode
        {
            public string name;
			public GameObject button;
            public float distanceBetweenLens;
        }
        public ToggleDisplayMode[] displayModes;

		public int currentDisplayMode;

        void OnEnable()
        {
			UpdateUI ();            
        }

		void UpdateUI() {
			for( int k=0; k<displayModes.Length; k++ )
			{
				if ((int)FibrumController.distanceBetweenLens == (int)displayModes [k].distanceBetweenLens) {
					displayModes [k].button.SetActive (true);
					currentDisplayMode = k;
				}
				else displayModes[k].button.SetActive( false );
			}
		}

		public void NextDisplayMode() {
			currentDisplayMode++;
			if (currentDisplayMode >= displayModes.Length) {
				currentDisplayMode = 0;
			}

			PlayerPrefs.SetFloat("FIB_lensDistance",FibrumController.distanceBetweenLens=displayModes[currentDisplayMode].distanceBetweenLens);
			UpdateUI ();
		}

        /*public void ToogleVRDevice_FullScreen(bool on)
        {
            if( displayModes[0].toggle.isOn ) PlayerPrefs.SetFloat("FIB_lensDistance",FibrumController.distanceBetweenLens=displayModes[0].distanceBetweenLens);
        }
        public void ToogleVRDevice_Fibrum(bool on)
        {
            if( displayModes[1].toggle.isOn ) PlayerPrefs.SetFloat("FIB_lensDistance",FibrumController.distanceBetweenLens=displayModes[1].distanceBetweenLens);
        }*/
    
    }
}
