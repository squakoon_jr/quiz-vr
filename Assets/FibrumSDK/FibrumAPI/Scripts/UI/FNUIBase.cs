﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.FibrumAPI.Scripts.UI
{
    public class FNUIBase : MonoBehaviour
    {
        public GameObject SettingsPanel;
        public GameObject FibrumClubPanel;
        public GameObject UpwardFCTitle;
        public GameObject FreemiumRoot;
        private GameObject tempEventSystem;
        public int AnimationFrames;
        public Texture SetupTex;
        public Texture lineTex;
        private bool isOpened;
        public RawImage mainUIgo;
        public CanvasGroup alphaPanel;
        public static FNUIBase Instance;
        private VR_canvasUIcontroller vrUIController;
        public Text AppVersionText;

        private void SetAppVersionText()
        {
            AppVersionText.text = string.Format("{0}v{1}", Application.productName, Application.version);
        }

        void Awake()
        {
            Debug.Log("FreemiumUIController Awake");
            if (Instance == null) Instance = this;
            else if (Instance != this) Destroy(gameObject.GetComponent(Instance.GetType()));
            DontDestroyOnLoad(gameObject);
            Init();
        }

        public void Init()
        {
            DontDestroyOnLoad(gameObject);
            FibrumAPI.Instance.BlockApplication += OnApplicationBlock;
            CheckEventSystem();
            SetAppVersionText();
        }

        public void CheckEventSystem()
        {
            if (vrUIController == null)
            {
                vrUIController = FindObjectOfType<VR_canvasUIcontroller>();
                if (vrUIController != null)
                    vrUIController.Init();
            }
        }

		string IsFirstTrialEndEvent()
		{
			string key = "Events_TrialEnd";

			if (PlayerPrefs.HasKey (key))
			{
				PlayerPrefs.SetString (key, false.ToString ());
			} 
			else
			{
				PlayerPrefs.SetString (key, true.ToString ());
			}

			return PlayerPrefs.GetString (key);
		}

        public void OnApplicationBlock(object source, FibrumAPI.AppBlockEventArgs e)
        {
            switch (e.AppBlockType)
            {
                case FibrumAPI.AppBlockEventArgs.BlockEventType.TrialEnd:
                    FNUIUserPanel.Instance.StartPanel();

                    string[] eventParams = new string[10];
                    eventParams[0] = "Trial";
                    eventParams[1] = "End";
                    FibrumAPIEvents.Instance.SendCustomEvent(27, eventParams, string.Empty);

				{
					eventParams = new string[10];
					eventParams [0] = "Trial";
					eventParams [1] = "End";
					eventParams [2] = IsFirstTrialEndEvent ();
					FibrumAPIEvents.Instance.SendCustomEvent (FibrumAPIEvents.EventType.Common, eventParams, string.Empty);
				}

                    break;
                case FibrumAPI.AppBlockEventArgs.BlockEventType.NetworkUnavailable:
                    break;
                case FibrumAPI.AppBlockEventArgs.BlockEventType.WaitingUserConfirm:
                    break;
                case FibrumAPI.AppBlockEventArgs.BlockEventType.HTTPRequestFail:
                    break;
            }

            //isOpened = true;
            if (!FreemiumRoot.gameObject.activeSelf)
            {
                UIPanelAction();
            }
        }

        public void OnCloseUpwardButtonclick()
        {
            CloseUIPanel();
            //isOpened = false;
        }

        public void OnFibrumClubToggleClick()
        {
            FibrumClubPanel.SetActive(true);
            SettingsPanel.SetActive(false);
        }

        public void OnSettinsToggleClick()
        { 
            SettingsPanel.SetActive(true);
            FibrumClubPanel.SetActive(false);
        }

        private void UIPanelAction()
        {
            StartCoroutine(_UIPanelAction());
        }

        private IEnumerator _UIPanelAction()
        {
            CheckEventSystem();
            Debug.Log("_UIPanelAction");
            if (!FreemiumRoot.gameObject.activeSelf)
            {
                FreemiumRoot.gameObject.SetActive(true);
				if (vrUIController)
					vrUIController.SetEventSystemState(true);
                for (int k = 0; k < AnimationFrames; k++)
                {
                    mainUIgo.color = Color.Lerp(new Color(0f, 0f, 0f, 0f), new Color(0f, 0f, 0f, 0.75f),
                        (float)k / ((float)AnimationFrames - 1));
                    alphaPanel.alpha = (float)(k - 10) / ((float)AnimationFrames - 1 - 10);
                    alphaPanel.transform.localScale = Vector3.Lerp(Vector3.one * 1.3f, Vector3.one,
                        (float)(k - 10) / ((float)AnimationFrames - 1 - 10));
                    yield return new WaitForEndOfFrame();
                }
            }
            else
            {
                for (int k = AnimationFrames - 1; k >= 0; k--)
                {
                    mainUIgo.color = Color.Lerp(new Color(0f, 0f, 0f, 0f), new Color(0f, 0f, 0f, 0.75f),
                        (float)k / ((float)AnimationFrames - 1));
                    alphaPanel.alpha = (float)(k - 10) / ((float)AnimationFrames - 1 - 10);
                    alphaPanel.transform.localScale = Vector3.Lerp(Vector3.one * 1.3f, Vector3.one,
                        (float)(k - 10) / ((float)AnimationFrames - 1 - 10));
                    yield return new WaitForEndOfFrame();
                }
                FreemiumRoot.gameObject.SetActive(false);
                vrUIController.SetEventSystemState(false);
            }
        }

        public void CloseUIPanel()
        {
            UIPanelAction();
        }


        private void OnGUI()
        {
            if (FreemiumRoot.activeSelf)
                return;

            GUI.DrawTexture(
                new Rect(Screen.width/2f - 1f, 0.01f*Screen.width, 2f,
                    Screen.height - 0.01f*Screen.width - 0.09f*Screen.width), lineTex);
            GUIStyle style = GUI.skin.GetStyle("label");
            style.alignment = TextAnchor.MiddleCenter;

            if (
                GUI.Button(
                    new Rect(Screen.width/2f - 0.04f*Screen.width, Screen.height - 0.08f*Screen.width,
                        0.08f*Screen.width, 0.08f*Screen.width), SetupTex, style))
            {
                isOpened = true;
                UIPanelAction();
                FNUIUserPanel.Instance.StartPanel();
            }
        }
    }
}
