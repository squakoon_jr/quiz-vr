﻿using System;
using System.IO;
using System.Text;
using UnityEngine;

namespace Assets.FibrumAPI.Scripts.FibrumLocalization
{
    public class LocalizationController// : MonoBehaviour
    {
        private string _localizationPath;
        private string _filePath;
        private string _defaultEngLocalizationPath;
        private string _startEngLocalizationPath = @"Localization/English";

        private string _localizationText;

        //private void Awake()
        //{
        //    FibrumAPI.InitializedEvent += OnInitializedEvent;
        //}

        //private void OnInitializedEvent(object sender, EventArgs e)
        //{
        //    FibrumAPI.InitializedEvent -= OnInitializedEvent;
        //    _localizationPath = Application.persistentDataPath + @"/Localization";
        //    Debug.Log(_localizationPath);
        //    _defaultEngLocalizationPath = Application.persistentDataPath + @"/Localization/English.txt";
        //    GetLocaliztion();
        //}

        public void Init()
        {
            _localizationPath = Application.persistentDataPath + @"/Localization";
            Debug.Log(_localizationPath);
            _defaultEngLocalizationPath = Application.persistentDataPath + @"/Localization/English.txt";
            GetLocaliztion();
        }

        private void OnFail()
        {
            if (File.Exists(_defaultEngLocalizationPath))
                _localizationText = File.ReadAllText(_defaultEngLocalizationPath);
            else
            {
                var textAsset = Resources.Load<TextAsset>(_startEngLocalizationPath);
                if (textAsset!=null)
                    _localizationText = textAsset.text;
                else
                {
                    throw new FileNotFoundException("Can't find default localization");
                }
            }

            FibrumLocalization.SetLanguage(_localizationText);
        }

        private void OnSuccess(string jsonData)
        {
            File.WriteAllText(_filePath, jsonData, Encoding.UTF8);
            _localizationText = File.ReadAllText(_filePath);
            FibrumLocalization.SetLanguage(_localizationText);
        }


        private void GetLocaliztion()
        {
            if (!Directory.Exists(_localizationPath))
            {
                Directory.CreateDirectory(_localizationPath);
            }

            if (!File.Exists(_defaultEngLocalizationPath))
            {
                FibrumAPI.Instance.StartCoroutine(FibrumAPI.Instance.GetLocalization(SystemLanguage.English,
                    s => 
                    {   
                        File.WriteAllText(_defaultEngLocalizationPath, s, Encoding.UTF8);
                        GetCurrentLocalization();
                    }
                    , OnFail));
                return;
            }

            GetCurrentLocalization();
        }

        private void GetCurrentLocalization()
        {
            _filePath = _localizationPath + @"/" + Application.systemLanguage + ".txt";

            if (!File.Exists(_filePath))
            {
                FibrumAPI.Instance.StartCoroutine(FibrumAPI.Instance.GetLocalization(Application.systemLanguage, OnSuccess, OnFail));
            }

            else
            {
                _localizationText = File.ReadAllText(_filePath);
                FibrumLocalization.SetLanguage(_localizationText);
            }
        }
    }
}
