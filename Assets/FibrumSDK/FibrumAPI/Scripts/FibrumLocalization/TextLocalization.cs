using UnityEngine;
using UnityEngine.UI;

namespace Assets.FibrumAPI.Scripts.FibrumLocalization
{
    [RequireComponent(typeof(Text))]
    public class TextLocalization : MonoBehaviour
    {
        public string Key;
        private Text label;

        public string value
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    if (label == null)
                    {
                        label = gameObject.GetComponent<Text>();
                    }
                    label.text = value;
#if UNITY_EDITOR
                    UnityEditor.EditorUtility.SetDirty(label);
#endif
                }
            }
        }

        private void Awake()
        {
            FibrumLocalization.LocalizationChanged += OnLocalize;
            label = gameObject.GetComponent<Text>();
        }

        private void Destroy()
        {
            FibrumLocalization.LocalizationChanged -= OnLocalize;
        }

        private void OnEnable()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
            OnLocalize();
        }

        private void Start()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
            OnLocalize();
        }

        public void OnLocalize()
        {
            if (string.IsNullOrEmpty(Key))
            {
                Key = label.text;
            }
            else
            {
                value = FibrumLocalization.Get(Key);
            }
        }

        public void SetKey(string newkey)
        {
            Key = newkey;
            OnLocalize();
        }
    }
}
