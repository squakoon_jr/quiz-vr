using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LitJson;
using Org.BouncyCastle.Utilities;
using UnityEngine;

namespace Assets.FibrumAPI.Scripts.FibrumLocalization
{
    public static class FibrumLocalization
    {
        public static event Localize LocalizationChanged;

        private static readonly Dictionary<string, string> dictionary = new Dictionary<string, string>();

        public static List<string> Keys { get { return new List<string>(dictionary.Keys); }}  

        public static void SetLanguage(string localizationJson)
        {
            JsonData data = JsonMapper.ToObject(localizationJson);
            if (data["status"].ToString() == "success")
            {
                foreach (var key in data["data"].Keys)
                {
                    dictionary.Add(key, data["data"][key].ToString().Replace(@"\n", "\n"));
                }
            }

            if (Application.isPlaying)
            {
                if(LocalizationChanged != null)
                    LocalizationChanged();
            }
        }

        public static string Get(string key)
        {
            string val;

            if (dictionary.TryGetValue(key, out val))
            {
                return val;
            }
            Debug.LogWarning("Localization key not found: '" + key + "'");
            return key;
        }

        public static string GetAll(string key)
        {
            string val;

            if (dictionary.TryGetValue(key, out val))
                return val;

#if UNITY_EDITOR
            Debug.LogWarning("Localization key not found: '" + key + "'");
#endif
            return null;
        }


        public static bool Exists(string key)
        {
            return dictionary.ContainsKey(key);
        }
    }

    public delegate void Localize();
}