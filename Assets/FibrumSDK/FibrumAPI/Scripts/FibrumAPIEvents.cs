﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using UnityEngine;

namespace Assets.FibrumAPI.Scripts
{
    public class FibrumAPIEvents : MonoBehaviour
    {
        public static FibrumAPIEvents Instance;

        public int MaxEventBatchSize;
        public float EventBatchSendRate;
        public bool LockSendEvents;

        public List<AppCustomEvent> EventBatchList = new List<AppCustomEvent>();

        private string cachePath;
        private float nextEventBatch;
        private bool Initialized;
        private DirectoryInfo cacheDirectoryInfo;
        public static Guid sessionUID;
        private float sessionTimer;

		public enum EventType
		{
			ViewFocus = 1,
			ObjectAction = 2,
			ObjectProcess = 3,
			HitMap = 4,
			SessionStart = 5,
			SessionEnd = 6,
			GameStart = 7,
			GameFinish = 8,
			Kills = 9,
			Info = 10,
			Login = 11,
			FBLogin = 12,
			BannerServerResponse = 13,
			Requested = 14,
			Received = 15,
			Decodeerror = 16,
			Downloaderror = 17,
			Shown = 18,
			Clicked = 19,
			Timeout = 20,
			Closed = 21,
			Downloaded = 22,
			Common = 23,
			Gamepad = 26,
			Registration = 27,
			TriesLeft = 30,
			FullVersion = 36,
			SphereOpen = 37,
			SphereClose = 38,
			AttractionStart = 39,
			AttractionEnd = 40,
            FlightStart = 41,
            FlightEnd = 42,
            TimeOnScreen = 43,
            LevelStart = 44,
            LevelFinish = 45,
            Checkpoint = 46,
            MultiplayerStart = 47,
            SkinsChange = 48
		};

        private void Awake()
        {
            FibrumAPIDebug.LogMessage(this, "Awake");
            if (Instance == null) Instance = this;
            else if (Instance != this) Destroy(gameObject.GetComponent(Instance.GetType()));
            DontDestroyOnLoad(gameObject);
            if (!Initialized) Init();
        }

        private void Init()
        {
            sessionUID = Guid.NewGuid();
            sessionTimer = 0;

            FibrumAPIDebug.LogMessage(this, "Init");
            cachePath = Application.persistentDataPath + @"/Cache";
            Debug.Log(cachePath);
            if (!Directory.Exists(cachePath))
            {
                Directory.CreateDirectory(cachePath);
            }
            cacheDirectoryInfo = new DirectoryInfo(cachePath);
        }

//        public  void StartSessionEvent()
//        {
//            string[] eventParams = new string[10];
//            eventParams[0] = sessionUID.ToString();
//            FibrumAPIEvents.Instance.SendCustomEvent(5, eventParams, string.Empty);
//        }

		public void StartSessionEvent()
		{
			FibrumAPIEvents.Instance.SendCustomEvent (EventType.SessionStart, new string[] { IsFirstSession () }, string.Empty);
		}

		string IsFirstSession()
		{
			string key = "Events_Session";

			if (!PlayerPrefs.HasKey (key))
			{
				PlayerPrefs.SetString (key, "FirstSession");
			}

			return PlayerPrefs.GetString (key);
		}

        private void CheckEventsCache()
        {
            var files = cacheDirectoryInfo.GetFiles();
            if (files.Length > 0)
            {
                FibrumAPIDebug.LogMessage(this, "Custom event cache found");
                StartCoroutine(SendCachedEvents(files));
            }
        }

		public void SendCustomEvent(EventType eventType, string[] eventParams, string advGuid)
		{
			if (eventParams.Length < 10)
			{
				string[] tempEventParams = new string[10];

				for (int i = 0; i < eventParams.Length; ++i)
				{
					tempEventParams [i] = eventParams [i];
				}

				eventParams = tempEventParams;
			}

			SendCustomEvent ((int)eventType, eventParams, advGuid);
		}

        public void SendCustomEvent(int eventType, string[] eventParams, string advGuid)
        {
			FibrumAPIDebug.LogMessage (this, "Event type: " + ((EventType)eventType).ToString ());

            AppCustomEvent appEvent = new AppCustomEvent
            {
                deviceToken = FibrumAPI.DeviceToken,
                eventType = eventType,
                sessionGuid = FibrumAPI.SessionGuid,
                appId = FibrumAPI.AppGuid,
                installationGuid = FibrumAPI.InstallationGuid,
                userGuid = FibrumAPI.UserGuid,
                advGuid = advGuid,
                param1 = eventParams[0],
                param2 = eventParams[1],
                param3 = eventParams[2],
                param4 = eventParams[3],
                param5 = eventParams[4],
                param6 = eventParams[5],
                param7 = eventParams[6],
                param8 = eventParams[7],
                param9 = eventParams[8],
                param10 = eventParams[9],
                createdAt = FibrumAPI.GetFormattedW3CDate()
            };
            EventBatchList.Add(appEvent);
        }

        private IEnumerator SendCachedEvents(FileInfo[] eventCache)
        {
            for (int i = 0; i < eventCache.Length; i++)
            {
                yield return
                    FibrumAPI.Instance.StartCoroutine(
                        FibrumAPI.Instance.SendCustomEventBatch(File.ReadAllText(eventCache[i].FullName),
                            () => { eventCache[i].Delete(); }, () => { }));
            }
        }

        private void WriteEventsCache()
        {
            FibrumAPIDebug.LogMessage(this, "Writing custom event batch");
            File.WriteAllText(cachePath + @"/ebatch" + Guid.NewGuid() + ".txt",
                JsonMapper.ToJson(EventBatchList.ToArray()));
        }

        void OnApplicationQuit()
        {
//            string[] eventParams = new string[10];
//            eventParams[0] = sessionUID.ToString();
//            eventParams[1] = ((int)sessionTimer).ToString();
//            FibrumAPIEvents.Instance.SendCustomEvent(6, eventParams, string.Empty);

			EndSessionEvent ();

            if (EventBatchList.Count > 0)
            {
                WriteEventsCache();
                EventBatchList.Clear();
            }
        }

		void EndSessionEvent()
		{
			string[] eventParams = new string[10];

			eventParams [0] = IsFirstSession ();
			eventParams[2] = ((int)sessionTimer).ToString();

			FibrumAPIEvents.Instance.SendCustomEvent ((int)EventType.SessionEnd, eventParams, string.Empty);

			PlayerPrefs.SetString ("Events_Session", "NextSession");
		}

        private void Update()
        {
            sessionTimer += Time.deltaTime;

            if (!FibrumAPI.NetworkAvailable) return;

            if (Time.time > nextEventBatch)
            {
                if (!LockSendEvents)
                {
                    nextEventBatch = Time.time + EventBatchSendRate;
                    CheckEventsCache();
                }
                else
                {
                    nextEventBatch = Time.time + EventBatchSendRate;
                }
            }

            if (EventBatchList.Count >= MaxEventBatchSize)
            {
                FibrumAPIDebug.LogMessage(this, "EventBatchList Count: " + EventBatchList.Count);
                WriteEventsCache();
                EventBatchList.Clear();
            }
        }

        [Serializable]
        public class AppCustomEventBatch
        {
            public AppCustomEventBatch(int length)
            {
                EventBatch = new AppCustomEvent[length];
            }

            public AppCustomEvent[] EventBatch;
        }

        [Serializable]
        public class AppCustomEvent
        {
            public string deviceToken;
            public int eventType;
            public string sessionGuid;
            public string appId;
            public string installationGuid;
            public string userGuid;
            public string advGuid;
            public string googleCID;
            public string param1;
            public string param2;
            public string param3;
            public string param4;
            public string param5;
            public string param6;
            public string param7;
            public string param8;
            public string param9;
            public string param10;
            public string createdAt;
        }
    }
}