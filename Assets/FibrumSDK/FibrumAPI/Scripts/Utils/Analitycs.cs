using System;
using UnityEngine;

namespace Assets.FibrumAPI.Scripts.Utils
{
    public class Analitycs : MonoBehaviour
    {
        public GoogleAnalyticsV4 componentAnalitycs;

        void Awake()
        {
            FibrumAPI.InitializedEvent += OnAPIInitialize;
        }

        public void OnAPIInitialize(object sender, EventArgs e)
        {
            Debug.Log("API Initialized, starting GA");
            FibrumAPI.InitializedEvent -= OnAPIInitialize;

            componentAnalitycs.StartSession();
            componentAnalitycs.SetUserIDOverride(FibrumAPI.UserGuid);
            componentAnalitycs.LogScreen(new AppViewHitBuilder().SetScreenName("Start screen").SetCustomDimension(1, FibrumAPI.DeviceToken));
            componentAnalitycs.LogScreen(new AppViewHitBuilder().SetScreenName("Start screen").SetCustomDimension(2, FibrumAPI.UserGuid));
        }

        void OnApplicationQuit()
        {
            componentAnalitycs.StopSession();
        }
    }
}