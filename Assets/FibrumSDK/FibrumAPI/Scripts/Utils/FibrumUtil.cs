﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Assets.FibrumAPI.Scripts.Utils
{
    public static class Utils
    {
        /// <summary>
        /// Regular expression, which is used to validate an E-Mail address.
        /// </summary>
        private const string MatchEmailPattern =
            @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
            + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

        /// <summary>
        /// Checks whether the given Email-Parameter is a valid E-Mail address.
        /// </summary>
        /// <param name="email">Parameter-string that contains an E-Mail address.</param>
        /// <returns>True, when Parameter-string is not null and 
        /// contains a valid E-Mail address;
        /// otherwise false.</returns>
        public static bool IsValidEmail(string email)
        {
            if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
            else return false;
        }

        public static class Encryption
        {
            public const string KEY = "txglX0SyxQe0qduqZbHFyQn9454uuq0HBoLhU3OiUlw=";
            public const string IV = "mBzw3hCXEmm4W5GXdJN8a+laeom5j2XrrNzw03CFCBQ=";

            public static string Encrypt(string prm_text_to_encrypt, string prm_key, string prm_iv)
            {
                var sToEncrypt = prm_text_to_encrypt;

                var rj = new RijndaelManaged()
                {
                    Padding = PaddingMode.PKCS7,
                    Mode = CipherMode.CBC,
                    KeySize = 256,
                    BlockSize = 256,
                };

                var key = Convert.FromBase64String(prm_key);
                var iv = Convert.FromBase64String(prm_iv);

                var encryptor = rj.CreateEncryptor(key, iv);

                var msEncrypt = new MemoryStream();
                var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

                var toEncrypt = Encoding.ASCII.GetBytes(sToEncrypt);

                csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
                csEncrypt.FlushFinalBlock();

                var encrypted = msEncrypt.ToArray();

                return (Convert.ToBase64String(encrypted));
            }

            public static string Decrypt(string prm_text_to_decrypt, string prm_key, string prm_iv)
            {

                var sEncryptedString = prm_text_to_decrypt;

                var rj = new RijndaelManaged()
                {
                    Padding = PaddingMode.PKCS7,
                    Mode = CipherMode.CBC,
                    KeySize = 256,
                    BlockSize = 256,
                };

                var key = Convert.FromBase64String(prm_key);
                var iv = Convert.FromBase64String(prm_iv);

                var decryptor = rj.CreateDecryptor(key, iv);

                var sEncrypted = Convert.FromBase64String(sEncryptedString);

                var fromEncrypt = new byte[sEncrypted.Length];

                var msDecrypt = new MemoryStream(sEncrypted);
                var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

                csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

                return (Encoding.ASCII.GetString(fromEncrypt));
            }

            public static void GenerateKeyIV(out string key, out string IV)
            {
                var rj = new RijndaelManaged()
                {
                    Padding = PaddingMode.PKCS7,
                    Mode = CipherMode.CBC,
                    KeySize = 256,
                    BlockSize = 256,
                };
                rj.GenerateKey();
                rj.GenerateIV();

                key = Convert.ToBase64String(rj.Key);
                IV = Convert.ToBase64String(rj.IV);
            }
        }

    }
}
