﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LibTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //AN_ProxyPool.CallStatic("com.fibrum.localnotifications", "generateNotification", args);
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass jng = new AndroidJavaClass("com.fibrum.localnotifications.NotificationGenerator");
        List<object> args = new List<object>();
        args.Add(jo);
        args.Add(0);
        args.Add("Test title");
        args.Add("Test subtitle");
        args.Add(@"http://api.fibrum.com");
        args.Add("Confirm");
        args.Add("Decline");
        jng.CallStatic("generateNotification", args.ToArray());
    }
}
