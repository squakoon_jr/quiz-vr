﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.FibrumAPI.Scripts.UI;
using UnityEngine;

namespace Assets.FibrumAPI.Scripts
{
    public class FibrumAPIBilling : MonoBehaviour
    {
        public static FibrumAPIBilling Instance;
        public static event EventHandler BillingReady;
        // public event EventHandler PurchaseRestored;
        public static event EventHandler FullGamePurchaseTargetUpdated;
        public UM_InAppProduct PurchaseTarget = null;
        public bool IsInitialized;

        void Awake()
        {
            if (Instance == null) Instance = this;
            else if (Instance != this) Destroy(gameObject.GetComponent(Instance.GetType()));
            DontDestroyOnLoad(gameObject);
            FibrumAPI.InitializedEvent += OnAPIInitialized;
            AndroidNativeSettings.Instance.InAppProducts.Clear();
            //IOSNativeSettings.Instance.InAppProducts.Clear();
        }

        public void FullGamePurchase()
        {
            FibrumAPIDebug.LogMessage(this, "Purchase target found:" + (PurchaseTarget != null));
            if (PurchaseTarget != null)
            {
#if UNITY_IOS

				IOSInAppPurchaseManager.Instance.BuyProduct (FibrumAPI.Instance.FullGameInApp);
				FibrumAPIDebug.LogMessage (this, "PurchaseTarget: " + FibrumAPI.Instance.FullGameInApp);
#endif

#if UNITY_ANDROID

                AndroidInAppPurchaseManager.ActionProductPurchased += OnActionProductPurchased;
                AndroidInAppPurchaseManager.Instance.Purchase(PurchaseTarget.AndroidTemplate.SKU);
                FibrumAPIDebug.LogMessage(this, "PurchaseTarget: " + PurchaseTarget.AndroidTemplate.SKU);
#endif
            }
        }

        public void PurchaseSelected(string inappID)
        {
            FibrumAPIDebug.LogMessage(this, "Purchasing inapp:" + inappID);
#if UNITY_IOS

			IOSInAppPurchaseManager.Instance.BuyProduct (inappID);
#endif

#if UNITY_ANDROID

            AndroidInAppPurchaseManager.ActionProductPurchased += OnActionProductPurchased;
            AndroidInAppPurchaseManager.Instance.Purchase(inappID);
#endif
            FibrumAPIDebug.LogMessage(this, "Trying purchase inapp: " + inappID);
        }

        public void UpdateFullGameInAppProduct()
        {
            if (string.IsNullOrEmpty(FibrumAPI.Instance.FullGameInApp))
            {
                Debug.Log("Full game inapp is null or empty!");
                return;
            }
            AddInAppProducts();
        }


        private void Connect()
        {
            if (FibrumAPI.NetworkAvailable)
            {
#if UNITY_IOS

                AddInAppProducts();//not really needed,but...only save ass))

                IOSInAppPurchaseManager.OnStoreKitInitComplete += OnStoreKitInitComplete;
                IOSInAppPurchaseManager.OnTransactionComplete += OnTransactionComplete;
                IOSInAppPurchaseManager.Instance.LoadStore();
#endif

#if UNITY_ANDROID
                AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnActionBillingSetupFinished;
                AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetrieveProducsFinished;
                AddInAppProducts();
#endif
            }
        }


        private void OnTransactionComplete(IOSStoreKitResult obj)
        {
            if (obj.IsSucceeded)
            {
                FibrumAPIDebug.LogMessage(this, "Purchase OK: " + obj.Receipt);
                if (!string.IsNullOrEmpty(obj.Receipt))
                {
                    //FibrumAPIDebug.LogMessage(this, "Android process buying");
                    //FibrumAPI.Instance.StartCoroutine(
                    //    FibrumAPI.Instance.SendIOSPurchase(obj.Receipt,
                    //        FNUIUserPanel.Instance.OnBillingBuy, () => { }));


                    FibrumAPIDebug.LogMessage(this, "iOS process buying");
                    switch (FibrumAPI.Instance.AppFreemiumType)
                    {
                        case FibrumAPI.FreemiumType.TrialLevel:
                        case FibrumAPI.FreemiumType.TriesCount:
                            FibrumAPI.Instance.StartCoroutine(
                            FibrumAPI.Instance.SendIOSPurchase(obj.Receipt,
                            FNUIUserPanel.Instance.OnBillingBuy, () => { }));
                            break;
                        case FibrumAPI.FreemiumType.CustomInApps:
                            FibrumAPI.Instance.StartCoroutine(
                            FibrumAPI.Instance.SendIOSPurchase(obj.Receipt,
                                () =>
                                {
                                    Debug.Log("Successfuly purchased: " + obj.ProductIdentifier);
                                    FibrumAPI.Instance.WriteInAppProductsData(obj.ProductIdentifier, true);
                                },
                                () =>
                                {
                                    Debug.Log("Failed purchasing: " + obj.ProductIdentifier);
                                }
                                ));
                            break;
                    }

                }
            }
            else
            {
                FibrumAPIDebug.LogMessage(this, "Purchase failed");
            }
        }

        private void OnStoreKitInitComplete(ISN_Result obj)
        {
            if (!obj.IsSucceeded) return;

            IsInitialized = true;
            //  AddInAppProducts(); //not where

            switch (FibrumAPI.Instance.AppFreemiumType)
            {
                case FibrumAPI.FreemiumType.TrialLevel:
                case FibrumAPI.FreemiumType.TriesCount:
                    foreach (var product in IOSInAppPurchaseManager.Instance.Products)
                    {
                        string description = string.Format("Product Description: {0}, ID: {1}, Price: {2}",
                        product.Description,
                        product.Id, product.LocalizedPrice);
                        Debug.Log(description);
                        if (product.Id == FibrumAPI.Instance.FullGameInApp)
                        {
                            Debug.Log("Setting Purchase Target:" + product.Id);
                            Debug.Log("Purchase Target LocalizedPrice:" + product.LocalizedPrice);
                            PurchaseTarget = new UM_InAppProduct();
                            PurchaseTarget.SetTemplate(product);
                            UM_InAppPurchaseManager.Instance.InAppProducts.Add(PurchaseTarget);
                            OnFullGamePurchaseTargetUpdated();
                        }
                    }
                    break;

                case FibrumAPI.FreemiumType.CustomInApps:
                    
                    foreach (var product in IOSInAppPurchaseManager.Instance.Products)
                    {
                        string description = string.Format("Product Description: {0}, ID: {1}, Price: {2}",
                        product.Description,
                        product.Id, product.LocalizedPrice);
                        Debug.Log(description);           
                        Debug.Log("Setting Purchase:" + product.Id);
                        IOSInAppPurchaseManager.Instance.AddProductId(product.Id);
                    }
                    break;
            }
            OnBillingReady();
        }

        private void OnBillingReady()
        {
            if (BillingReady != null)
            {
                BillingReady(this, new EventArgs());
            }
        }

        private void AddInAppProducts()
        {
#if UNITY_IOS

            switch (FibrumAPI.Instance.AppFreemiumType)
            {
                case FibrumAPI.FreemiumType.TrialLevel:
                case FibrumAPI.FreemiumType.TriesCount:
                    //if (IOSNativeSettings.Instance.InAppProducts.Any(a=>a.Id==fgInApp)) return;
                    //IOSInAppPurchaseManager.Instance.AddProductId(fgInApp);
                    if (IOSNativeSettings.Instance.InAppProducts.Any(a => a.Id == FibrumAPI.Instance.FullGameInApp))

                    {
                    }
                    else
                    {

                        var t = new IOSProductTemplate();
                        t.Id = FibrumAPI.Instance.FullGameInApp;
                        IOSNativeSettings.Instance.InAppProducts.Add(t);

                        var t2 = new UM_InAppProduct();
                        t2.id = FibrumAPI.Instance.FullGameInApp;
                        t2.SetTemplate(t);
                        UltimateMobileSettings.Instance.AddProduct(t2);
                    }
                    break;
                case FibrumAPI.FreemiumType.CustomInApps:
                    foreach (var inappID in FibrumAPI.Instance.InAppProductIDs)
                    {
                        if (IOSNativeSettings.Instance.InAppProducts.Any(a => a.Id == inappID))
                        {
                            Debug.Log("try add duplicate inAppID");
                        }
                        else
                        {
                            var pTemplate = new IOSProductTemplate();
                            pTemplate.Id = inappID;
                            IOSNativeSettings.Instance.InAppProducts.Add(pTemplate);

                            var t2 = new UM_InAppProduct();
                            t2.id = inappID;
                            t2.SetTemplate(pTemplate);
                            UltimateMobileSettings.Instance.AddProduct(t2);
                        }
                    }
                    break;
            }

            IOSInAppPurchaseManager.Instance.LoadStore();
#endif


          

#if UNITY_ANDROID
            switch (FibrumAPI.Instance.AppFreemiumType)
            {
                case FibrumAPI.FreemiumType.TrialLevel:
                case FibrumAPI.FreemiumType.TriesCount:
                    if (AndroidNativeSettings.Instance.InAppProducts.Any(a => a.SKU == FibrumAPI.Instance.FullGameInApp)) return;
                    var t = new GoogleProductTemplate();
                    t.SKU = FibrumAPI.Instance.FullGameInApp;
                    AndroidNativeSettings.Instance.InAppProducts.Add(t);
                    break;
                case FibrumAPI.FreemiumType.CustomInApps:
                    foreach (var inappID in FibrumAPI.Instance.InAppProductIDs)
                    {
                        var pTemplate = new GoogleProductTemplate();
                        pTemplate.SKU = inappID;
                        AndroidNativeSettings.Instance.InAppProducts.Add(pTemplate);
                    }
                    break;
            }
            AndroidInAppPurchaseManager.Instance.LoadStore();
#endif
        }

        private void OnAPIInitialized(object sender, EventArgs e)
        {
            FibrumAPIDebug.LogMessage(this, "API Initialized, starting");
            Connect();
        }

        private void OnFullGamePurchaseTargetUpdated()
        {
            Debug.Log("APIBilling:OnFullGamePurchaseTargetUpdated");
            if (FullGamePurchaseTargetUpdated != null)
            {
                FullGamePurchaseTargetUpdated(this, new EventArgs());
            }
        }

        private IEnumerator RestoreMultiplePurchases()
        {
#if UNITY_ANDROID
            foreach (var item in AndroidInventory)
            {
                string inappID = item.SKU;
                yield return
                    FibrumAPI.Instance.StartCoroutine(FibrumAPI.Instance.SendAndroidPurchase(item.originalJson,
                        () =>
                        {
                            Debug.Log("Successfuly restored purchase: " + inappID);
                            FibrumAPI.Instance.WriteInAppProductsData(inappID, true);
                        }, 
                        () =>
                        {
                            Debug.Log("Failed restoring purchase: " + inappID);
                        }));
            }
#else
            yield break;
#endif
        }


        public void Restore()
        {
            Debug.Log("Restoring purchases"); 
#if UNITY_IOS

			IOSInAppPurchaseManager.OnRestoreComplete += OnRestoreComplete;
			IOSInAppPurchaseManager.Instance.RestorePurchases();
#endif

#if UNITY_ANDROID
            Debug.Log("AndroidInventory is null:"+(AndroidInventory==null));

			if (AndroidInventory.Count > 0)
			{
                switch (FibrumAPI.Instance.AppFreemiumType)
                {
                    case FibrumAPI.FreemiumType.TrialLevel:
                    case FibrumAPI.FreemiumType.TriesCount:
                        if (!string.IsNullOrEmpty(AndroidInventory[0].originalJson))
                        {
                            FibrumAPI.Instance.StartCoroutine(
                            FibrumAPI.Instance.SendAndroidPurchase(AndroidInventory[0].originalJson,
                            FNUIUserPanel.Instance.OnBillingRestore, () => { }));
                        }

                        break;
                    case FibrumAPI.FreemiumType.CustomInApps:
                        StartCoroutine(RestoreMultiplePurchases());
                        break;
                }
            }
#endif
        }

#if UNITY_IOS
        private void OnRestoreComplete(IOSStoreKitRestoreResult obj)
        {
            FibrumAPIDebug.LogMessage(this, "Restore OK: " + obj.IsSucceeded);
            IOSInAppPurchaseManager.OnRestoreComplete -= OnRestoreComplete;
        }
#endif

#if UNITY_ANDROID
        private void OnRetrieveProducsFinished(BillingResult obj)
        {
            // AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetrieveProducsFinished;
            IsInitialized = true;

            switch (FibrumAPI.Instance.AppFreemiumType)
            {
                case FibrumAPI.FreemiumType.TrialLevel:
                case FibrumAPI.FreemiumType.TriesCount:
                    if (string.IsNullOrEmpty(FibrumAPI.Instance.FullGameInApp)) return;
                    Debug.Log("OnRetrieveProducsFinished:Listing products");         
                    foreach (var product in AndroidInAppPurchaseManager.Instance.Inventory.Products)
                    {
                        string description = string.Format("Product Description: {0}, ID: {1}, Price: {2}",
                        product.Description,
                        product.SKU, product.LocalizedPrice);
                        Debug.Log(description);
                        if (product.SKU == FibrumAPI.Instance.FullGameInApp)
                        {
                            Debug.Log("Setting Purchase Target:" + product.SKU);
                            Debug.Log("Purchase Target LocalizedPrice:" + product.LocalizedPrice);
                            PurchaseTarget = new UM_InAppProduct();
                            PurchaseTarget.SetTemplate(product);
                            UM_InAppPurchaseManager.Instance.InAppProducts.Add(PurchaseTarget);
                            OnFullGamePurchaseTargetUpdated();
                        }
                    }
                    break;
                case FibrumAPI.FreemiumType.CustomInApps:
                    Debug.Log("OnRetrieveProducsFinished:Listing products");
                    foreach (var product in AndroidInAppPurchaseManager.Instance.Inventory.Products)
                    {
                        string description = string.Format("Product Description: {0}, ID: {1}, Price: {2}",
                        product.Description,
                        product.SKU, product.LocalizedPrice);
                        Debug.Log(description);
                        //GoogleProductTemplate inapp = new UM_InAppProduct();
                        //inapp.SetTemplate(product);
                        Debug.Log("Setting Purchase:" + product.SKU);
                        AndroidInAppPurchaseManager.Instance.AddProduct(product.SKU);
                        //UM_InAppPurchaseManager.Instance.InAppProducts.Add(inapp);
                    }
                    break;
            }
            OnBillingReady();
        }

        public List<GooglePurchaseTemplate> AndroidInventory
        {
            get { return AndroidInAppPurchaseManager.Instance.Inventory.Purchases; }
        }

        void OnActionBillingSetupFinished(BillingResult result)
        {
            AndroidInAppPurchaseManager.Instance.RetrieveProducDetails();
        }

        private void OnActionProductPurchased(BillingResult obj)
        {
            AndroidInAppPurchaseManager.ActionProductPurchased -= OnActionProductPurchased;
            if (obj.isSuccess)
            {
                FibrumAPIDebug.LogMessage(this, "Purchase OK: " + obj.purchase.originalJson);
                if (!string.IsNullOrEmpty(obj.purchase.originalJson))
                {
                    FibrumAPIDebug.LogMessage(this, "Android process buying");
                    switch (FibrumAPI.Instance.AppFreemiumType)
                    {
                        case FibrumAPI.FreemiumType.TrialLevel:
                        case FibrumAPI.FreemiumType.TriesCount:
                            FibrumAPI.Instance.StartCoroutine(
                            FibrumAPI.Instance.SendAndroidPurchase(obj.purchase.originalJson,
                            FNUIUserPanel.Instance.OnBillingBuy, () => { }));
                            break;
                        case FibrumAPI.FreemiumType.CustomInApps:
                            FibrumAPI.Instance.StartCoroutine(
                            FibrumAPI.Instance.SendAndroidPurchase(obj.purchase.originalJson,
                                () =>
                                {
                                    Debug.Log("Successfuly purchased: " + obj.purchase.SKU);
                                    FibrumAPI.Instance.WriteInAppProductsData(obj.purchase.SKU, true);
                                },
                                () =>
                                {
                                    Debug.Log("Failed purchasing: " + obj.purchase.SKU);
                                }
                                ));
                            break;
                    }
                }
            }
            else
            {
                FibrumAPIDebug.LogMessage(this, "Purchase failed");
            }
        }
#endif
    }
}