﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using Assets.FibrumAPI.Scripts.UI;

public class VR_canvasUIcontroller : MonoBehaviour {

	//Transform vrCamera;
	Camera UI_dummyCamera;
	public float lookToPressTime=2f;
	public Sprite defaultProgressBarTex;
	public Texture defaultPointerTex;
    private EventSystem sceneEventSystem;

    public void UpdatesSceneCanvases()
    {
        UI_dummyCamera = transform.Find("VRCamera/VR_UI_dummyCamera").GetComponent<Camera>();
        Canvas[] cvs = GameObject.FindObjectsOfType<Canvas>();
        for (int k = 0; k < cvs.Length; k++)
        {
            if (cvs[k].renderMode == RenderMode.WorldSpace) cvs[k].worldCamera = UI_dummyCamera;
        }
    }

    public void CheckEventSystem()
    {
        if (FindObjectOfType<EventSystem>() == null)
        {
            sceneEventSystem =
                Instantiate(Resources.Load("FibrumResources/EventSystem") as GameObject).GetComponent<EventSystem>();
        }
        else
        {
            sceneEventSystem = FindObjectOfType<EventSystem>();
          
        }

        if (sceneEventSystem.gameObject.GetComponent<VRInputModule>() == null)
        {
            var vrim = sceneEventSystem.gameObject.AddComponent<VRInputModule>();
            SetVRInputModuleGraphics(vrim);
        }
        else
        {
            var vrim = sceneEventSystem.gameObject.GetComponent<VRInputModule>();
            SetVRInputModuleGraphics(vrim);
        }
    }

    private void SetVRInputModuleGraphics(VRInputModule vrim)
    {
        vrim.timeToLookPress = lookToPressTime;
        vrim.SetProgressBarTexture(defaultProgressBarTex);
        vrim.SetVRpointerTexture(defaultPointerTex);
    }

    public void SetEventSystemState(bool isUIOpened)
    {
        if (FNUIBase.Instance != null)
        {
            sceneEventSystem.GetComponent<StandaloneInputModule>().enabled = isUIOpened;
            sceneEventSystem.GetComponent<VRInputModule>().enabled = !isUIOpened;
        }
    }

    void Start()
    {
       Init();
     
    }

    public void Init()
    {
        CheckEventSystem();
        UpdatesSceneCanvases();
        //SetEventSystemState(false);
    }
}
