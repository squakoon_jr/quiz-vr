﻿using UnityEngine;
using System.Collections;

public class KeyChainDebug : MonoBehaviour
{
	string appKey = "application key";
	string data = "user data";
	string groupAccess = "VMQN9AAD4C.com.fibrumApp";

	void Awake()
	{
		DontDestroyOnLoad (this.gameObject);
	}

	void OnGUI()
	{
		GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
		{
			GUILayout.Label("myKey");
			appKey = GUILayout.TextField(appKey,GUILayout.Height(Screen.height/8));
			GUILayout.Label("myData");
			data = GUILayout.TextField(data,GUILayout.Height(Screen.height/8));
			GUILayout.Label("group");
			groupAccess = GUILayout.TextField(groupAccess,GUILayout.Height(Screen.height/8));



			if (GUILayout.Button("Set",GUILayout.Height(Screen.height/8)))
				KeyChainBinding.ag_SetKeyChainData(appKey, data,groupAccess);
			if (GUILayout.Button("Get",GUILayout.Height(Screen.height/8)))
			{
				string applicationKey, retrievedData;
				KeyChainBinding.GetKeyChainData_AG(out applicationKey, out retrievedData,groupAccess);
				Debug.Log(string.Format("retrieved key: \"{0}\" retrieved data: \"{1}\"", applicationKey, retrievedData));
			}
			if (GUILayout.Button("Reset",GUILayout.Height(Screen.height/8)))
				KeyChainBinding.ag_DeleteKeyChainData(groupAccess);
		}
		GUILayout.EndArea();
	}
}