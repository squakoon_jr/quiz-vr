#import "KeyChainPlugin.h"
#import "UICKeyChainStore.h"
 
NSString *_keyForID = @"UserID";
NSString *_keyForUUID = @"UserUUID";
 
@implementation KeyChainPlugin
 
extern "C" {
    char* getKeyChainUser();
    void setKeyChainUser(const char* userId, const char* uuid);
    void deleteKeyChainUser();

    char* ag_getKeyChainUser(const char* accessGroup);
    void ag_setKeyChainUser(const char* userId, const char* uuid, const char* accessGroup);
    void ag_deleteKeyChainUser(const char* accessGroup);
}
 
char* getKeyChainUser()
{
    NSString *userId = [UICKeyChainStore stringForKey:_keyForID];
    NSString *userUUID = [UICKeyChainStore stringForKey:_keyForUUID];
 
    if (userId == nil || [userId isEqualToString:@""]) {
        NSLog(@"???????");
        userId = @"";
        userUUID = @"";
    }
 
    NSString* outputString = [NSString stringWithFormat:@"%@|%@",userId,userUUID];
	
    return makeStringCopy([outputString UTF8String]);
}

char* ag_getKeyChainUser(const char* accessGroup) 
{
    NSString *_accessGroup = [NSString stringWithCString:accessGroup encoding:NSUTF8StringEncoding];
    
    UICKeyChainStore *keychain = [UICKeyChainStore keyChainStoreWithService:@"kim.git" accessGroup:_accessGroup];
    
    NSString *userId = [keychain stringForKey:_keyForID];
    NSString *userUUID = [keychain stringForKey:_keyForUUID];
 
    if (userId == nil || [userId isEqualToString:@""]) {
        NSLog(@"???????");
        userId = @"";
        userUUID = @"";
    }
 
    NSString* outputString = [NSString stringWithFormat:@"%@|%@",userId,userUUID];
    
    return makeStringCopy([outputString UTF8String]);
}
 
void setKeyChainUser(const char* userId, const char* uuid)
{
    NSString *nsUseId = [NSString stringWithCString: userId encoding:NSUTF8StringEncoding];
    NSString *nsUUID = [NSString stringWithCString: uuid encoding:NSUTF8StringEncoding];
    
    
    [UICKeyChainStore setString:nsUseId forKey:_keyForID];
    [UICKeyChainStore setString:nsUUID forKey:_keyForUUID];
}

void ag_setKeyChainUser(const char* userId, const char* uuid, const char* accessGroup) 
{
    NSString *nsUseId = [NSString stringWithCString: userId encoding:NSUTF8StringEncoding];
    NSString *nsUUID = [NSString stringWithCString: uuid encoding:NSUTF8StringEncoding];
    NSString *_accessGroup = [NSString stringWithCString:accessGroup encoding:NSUTF8StringEncoding];
    
    UICKeyChainStore *keychain = [UICKeyChainStore keyChainStoreWithService:@"kim.git" accessGroup:_accessGroup];

    [keychain setString:nsUseId forKey:_keyForID];
    [keychain setString:nsUUID forKey:_keyForUUID];
}
 
void deleteKeyChainUser()
{
    [UICKeyChainStore removeItemForKey:_keyForID];
    [UICKeyChainStore removeItemForKey:_keyForUUID];
}

void ag_deleteKeyChainUser(const char* accessGroup)
{
    NSString *_accessGroup = [NSString stringWithCString:accessGroup encoding:NSUTF8StringEncoding];
    
    UICKeyChainStore *keychain = [UICKeyChainStore keyChainStoreWithService:@"kim.git" accessGroup:_accessGroup];
    
    [keychain removeItemForKey:_keyForID];
    [keychain removeItemForKey:_keyForUUID];
}
 
char* makeStringCopy(const char* str)
{
    if (str == NULL) {
        return NULL;
    }
 
    char* res = (char*)malloc(strlen(str) + 1);
    strcpy(res, str);
    return res;
}
 
@end