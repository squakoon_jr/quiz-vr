﻿using UnityEngine;
using System.Collections;
using BrainCloud;
#if UNITY_IOS
using NotificationServices = UnityEngine.iOS.NotificationServices;
using NotificationType = UnityEngine.iOS.NotificationType;
#endif

namespace BrainCloudUnity
{
    public class BrainCloudLoginPF : MonoBehaviour
    {
        private bool tokenSent;

        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        void Start()
        {


            BrainCloudWrapper.Initialize();
            BrainCloudWrapper.GetInstance().AuthenticateAnonymous(OnAnonymSuccessAuth, OnAnonymFailureAuth);

#if UNITY_ANDROID
            GoogleCloudMessageService.Instance.RgisterDevice();
            GoogleCloudMessageService.ActionCMDRegistrationResult += StartPush;
            GoogleCloudMessageService.ActionCouldMessageLoaded += OnMessageLoaded;
            GoogleCloudMessageService.Instance.InitPushNotifications();
            GoogleCloudMessageService.Instance.RemoveLastMessageInfo();
#endif

#if UNITY_IOS

            tokenSent = false;

		     Invoke("AskPush", 5f);
#endif
        }

#if UNITY_IOS
        void AskPush()
        {
            NotificationServices.RegisterForNotifications(
                NotificationType.Alert |
                NotificationType.Badge |
                NotificationType.Sound);
        }
#endif




#if UNITY_IOS
        void Update()
        {
            if (!tokenSent)
            {
                byte[] token = NotificationServices.deviceToken;
                if (token != null)
                {
                    // send token to a provider
                    string hexToken = "" + System.BitConverter.ToString(token).Replace("-", "");

                    BrainCloudPushNotification push = BrainCloudWrapper.GetBC().GetPushNotificationService();
                    Debug.Log("hexToken: "+hexToken);
                    push.RegisterPushNotificationDeviceToken(hexToken, Callback_Success, Callback_Failure);
                    tokenSent = true;
                }
            }
        }
#endif



#if UNITY_ANDROID

        private void OnMessageLoaded(string lastMessage)
        {
            Debug.Log(lastMessage);
        }

        void StartPush(GP_GCM_RegistrationResult result)
        {
            Debug.Log("GCM Registration has failed: " + result.isFailure);
            Debug.Log(result.RegistrationDeviceId);
            BrainCloudPushNotification push = BrainCloudWrapper.GetBC().GetPushNotificationService();
            string senderid = GoogleCloudMessageService.Instance.registrationId;
            Debug.Log(senderid);
            push.RegisterPushNotificationDeviceToken(senderid, Callback_Success, Callback_Failure);
        }
#endif


        private void Callback_Success(string response, object obj)
        {
            Debug.Log("RegisterPushNotification Success!");
            Debug.Log(response);
        }

        private void Callback_Failure(int status, int reasonCode, string jsonError, object cbObject)
        {
            Debug.Log("RegisterPushNotification Failure!");
            Debug.Log(jsonError);
        }

        public void OnAnonymSuccessAuth(string response, object obj)
        {
            Debug.Log("BrainCloud Anonymous Auth Success!");
        }

        public void OnAnonymFailureAuth(int status, int reasonCode, string jsonError, object cbObject)
        {
            Debug.Log("BrainCloud Anonymous Auth Failure!");
        }
    }
}

      