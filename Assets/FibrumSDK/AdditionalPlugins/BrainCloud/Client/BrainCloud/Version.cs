//----------------------------------------------------
// brainCloud client source code
// Copyright 2015 bitHeads, inc.
//----------------------------------------------------

namespace BrainCloud
{
    public class Version
    {
        public static string GetVersion()
        {
            return "2.21.0";
        }
    }
}
