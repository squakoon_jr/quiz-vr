﻿using UnityEngine;
using System.Collections;
using VoxelBusters.NativePlugins;

public class WebViewController : MonoBehaviour
{
	#if USES_WEBVIEW
	static WebViewController _instance = null;
	public static WebViewController Instance { get { return _instance; } }

	#pragma warning disable
	[SerializeField]
	WebView webView;
	[SerializeField]
	UnityEngine.UI.Image imgLoading;
	[SerializeField]
	GameObject canvasObj;
	[SerializeField]
	UnityEngine.UI.Text txtStatus;
	#pragma warning restore

	void Start ()
	{
		if (_instance == null)
		{
			_instance = this;
			DontDestroyOnLoad(this.gameObject);

			webView.AutoShowOnLoadFinish = true;
			webView.ControlType	= eWebviewControlType.CLOSE_BUTTON;
			webView.SetFullScreenFrame();
		}
		else
		{
			if (this != _instance)
				DestroyImmediate(this.gameObject);
		}
	}

	public void Open(string url)
	{
		canvasObj.SetActive (true);
		imgLoading.enabled = true;
		imgLoading.fillAmount = 0;
		txtStatus.text = "loading...";
		StartCoroutine (LoadingUI(2));

		webView.LoadRequest(url);
	}

	IEnumerator LoadingUI(float loading_time)
	{
		while (imgLoading.fillAmount < 0.95f) 
		{
			imgLoading.fillAmount += Time.fixedDeltaTime / loading_time;
			yield return new WaitForSeconds (Time.fixedDeltaTime);
		}
		txtStatus.text = "Bad Connection!";
		yield return new WaitForSeconds (2);
		imgLoading.enabled = false;
		canvasObj.SetActive (false);
	}

	private void OnEnable()
	{
		// Registering for event
		WebView.DidStartLoadEvent += DidStartLoadEvent;
		WebView.DidFinishLoadEvent += DidFinishLoadEvent;
		WebView.DidFailLoadWithErrorEvent += DidFailLoadWithErrorEvent;
	}

	private void OnDisable()
	{
		// Unregistering event
		WebView.DidStartLoadEvent -= DidStartLoadEvent;
		WebView.DidFinishLoadEvent -= DidFinishLoadEvent;
		WebView.DidFailLoadWithErrorEvent -= DidFailLoadWithErrorEvent;
	}

	private void DidStartLoadEvent(WebView _webview)
	{
		if (this.webView == _webview)
		{
			Debug.Log("Webview did start loading request.");
		}
	}

	private void DidFinishLoadEvent(WebView _webview)
	{
		if (this.webView == _webview)
		{
			Debug.Log("Webview did finish loading request.");
		}
		imgLoading.enabled = false;
		canvasObj.SetActive (false);
	}

	private void DidFailLoadWithErrorEvent(WebView _webview, string _error)
	{
		if (this.webView == _webview)
		{
			Debug.Log("Webview did fail with error: " + _error);
		}
		imgLoading.enabled = false;
		canvasObj.SetActive (false);
	}
	#endif
}
