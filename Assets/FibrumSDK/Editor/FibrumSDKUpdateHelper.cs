﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

public class FibrumSDKUpdateHelper
{

    public static List<DeleteObj> paths;
    [ExecuteInEditMode]
    [MenuItem("FibrumUtils/Try Clear Old SDK")]
    static void DoSomething()
    {
        paths = new List<DeleteObj>();
        //   DeleteObj del_obj = new DeleteObj("Best HTTP (Pro)", DeleteType.Folder);
        paths.Add(new DeleteObj("Best HTTP (Pro)", DeleteType.Folder));
        paths.Add(new DeleteObj("BrainCloud", DeleteType.Folder));
        paths.Add(new DeleteObj("Extensions", DeleteType.Folder));
        paths.Add(new DeleteObj("FibrumAPI", DeleteType.Folder));
        paths.Add(new DeleteObj("Media", DeleteType.Folder));
        paths.Add(new DeleteObj("iOSKeyChainPlugin", DeleteType.Folder));
        paths.Add(new DeleteObj("Resources/FibrumResources", DeleteType.Folder));
        paths.Add(new DeleteObj("Resources/Localization", DeleteType.Folder));
     //   paths.Add(new DeleteObj("Scenes/Ads.unity", DeleteType.File));
     //   paths.Add(new DeleteObj("Scenes/Scene1.unity", DeleteType.File));
     //   paths.Add(new DeleteObj("Scenes/Start.unity", DeleteType.File));
        paths.Add(new DeleteObj("WebPlayerTemplates", DeleteType.Folder));
        //plugins
        paths.Add(new DeleteObj("Plugins/BaseLiarary.dll", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/GoogleAdsWP8.dll", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/myCompiledLibrary.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/WP8Native.dll", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/WP8PopUps.dll", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/androidnative.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/brainCloudUnity.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/EasyMovieTexture.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/fibrumsdk.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/mobilenativepopups.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/myCompiledLibrary.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/play-services-base-9.8.0.aar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/play-services-basement-9.8.0.aar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/play-services-tasks-9.8.0.aar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/sensor.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/support-annotations-24.0.0.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/support-v4-24.0.0.aar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/libs/android-support-v4.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/libs/google-play-services.jar", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/libs/x86/libvrunity.so", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/libs/armeabi-v7a/libvrunity.so", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/Android/res", DeleteType.Folder));
        //paths.Add(new DeleteObj("Plugins/Editor", DeleteType.Folder));
        //paths.Add(new DeleteObj("Plugins/GoogleAnalyticsV4", DeleteType.Folder));
        //paths.Add(new DeleteObj("Plugins/StansAssets", DeleteType.Folder));
        //paths.Add(new DeleteObj("Plugins/Metro", DeleteType.Folder));
        //paths.Add(new DeleteObj("Plugins/WP8", DeleteType.Folder));
        paths.Add(new DeleteObj("Plugins/iOS/GADUAdLoader.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUAdLoader.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUBanner.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUBanner.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUInterface.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUInterstitial.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUInterstitial.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUNativeCustomTemplateAd.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUNativeCustomTemplateAd.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUNativeExpressAd.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUNativeExpressAd.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUObjectCache.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUObjectCache.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUPluginUtil.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUPluginUtil.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADURequest.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADURequest.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADURewardBasedVideoAd.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADURewardBasedVideoAd.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GADUTypes.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAI.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAIDictionaryBuilder.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAIEcommerceFields.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAIEcommerceProduct.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAIEcommerceProductAction.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAIEcommercePromotion.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAIFields.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAIHandler.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAIHandler.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAILogger.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAITrackedViewController.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/GAITracker.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/iCadeReaderView.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/iCadeReaderView.m", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/iCadeState.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/iCadeUnityLink.h", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/iCadeUnityLink.mm", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/libGoogleAnalyticsServices.a", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/RegionLocaleNative.mm", DeleteType.File));
        paths.Add(new DeleteObj("Plugins/iOS/UM_IOS_INSTALATION_MARK.txt", DeleteType.File));



        Debug.Log("Start Delete");

        string rootFolderPath = Application.dataPath + "/" + "Plugins/iOS/";
        string filesToDelete = @"ISN_*";   // Only delete DOC files containing "DeleteMe" in their filenames
        if (Directory.Exists(rootFolderPath))
        {
            string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
            foreach (string file in fileList)
            {
                System.Diagnostics.Debug.WriteLine(file + "will be deleted");
                File.Delete(file);
            }
        }






        foreach (DeleteObj item in paths)
        {
            switch (item.deleteType)
            {
                case DeleteType.Folder:
                    if (Directory.Exists(item.path))
                    {
                        //if it doesn't, create it
                        Directory.Delete(item.path, true);

                    }
                    break;
                case DeleteType.File:
                    if (File.Exists(item.path))
                    {
                        //if it doesn't, create it
                        File.Delete(item.path);
                    }
                    break;
            }
        }

        AssetDatabase.Refresh();

        Debug.Log("Finish Delete");
    }

    public enum DeleteType
    {
        Folder,
        File
    }

    public class DeleteObj
    {
        public string path;
        public DeleteType deleteType;

        public DeleteObj(string path, DeleteType deleteType)
        {
            this.path = Application.dataPath + "/" + path;
            this.deleteType = deleteType;
        }
    }
}
