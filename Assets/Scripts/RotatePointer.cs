﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePointer : MonoBehaviour 
{
    public Transform aim;

    static RotatePointer instance;

    static public RotatePointer Instance
    {
        get
        {
            return instance;
        }
    }


    Transform target;

    public Transform Target
    {
        set
        {
            target = value;

            if (target == null)
            {
                transform.localEulerAngles = Vector3.zero;
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, 0);

                aim.gameObject.SetActive(false);
                aim.SetParent(transform.parent);
                aim.localEulerAngles = Vector3.zero;
                aim.localPosition = Vector3.zero;

            }
            else if(Gestures.rotateMode != Gestures.RotateMode.Free && Gestures.rotateMode != Gestures.RotateMode.FreeAndZ)
            {
                aim.gameObject.SetActive(true);
                aim.localEulerAngles = Vector3.zero;
                aim.position = target.position;
                aim.SetParent(null);
                aim.localScale = Vector3.one * 0.15f * Vector3.Distance(aim.position, VRCamera.Instance.transform.position);
                aim.eulerAngles = new Vector3(aim.eulerAngles.x, aim.eulerAngles.y, 0);
            }
        }
    }

	// Use this for initialization
	void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        if (target)
        {
            transform.LookAt(target);
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, Vector3.Distance(transform.position, target.position) / transform.parent.localScale.z);
        }
    }
}
