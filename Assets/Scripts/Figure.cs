﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Figure : MonoBehaviour 
{
    public event System.Action CompleteEvent;

    public Transform target;

    public bool rotateMode = false;
    public bool moveMode = false;
    bool canDragged = true;

    public bool isSelected = false;

    static bool isAnyActive = false;
    static Figure activeFigure;

    public float activeDistance = 2f;
    bool isCompleted = false;

    Color originalColor;
    Material mat;

    void Start()
    {
        //mat = GetComponent<Renderer>().sharedMaterial;
        //originalColor = mat.color;

        Gestures.TiltHeadLeft += Grab;
        Gestures.TiltHeadRight += SwitchRotateMode;
    }

    void OnDestroy()
    {
        Gestures.TiltHeadLeft -= Grab;
        Gestures.TiltHeadRight -= SwitchRotateMode;
    }

    public void OnComplete()
    {
        if (CompleteEvent != null && !isCompleted)
        {
            isCompleted = true;
            CompleteEvent();
        }
    }

    bool CheckFocus()
    {
        Vector3 dir = VRCamera.Instance.transform.GetChild(0).forward;

        RaycastHit hit;

        if (Physics.Raycast(VRCamera.Instance.transform.position, dir, out hit, 5))
        {
            if (hit.transform == transform)
            {
                return true;
            }
        }

        return false;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.transform.Equals(target) && Vector2.Angle(target.forward, transform.forward) < 10f)
        {
            AutoComplete();
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.transform.Equals(target) && canDragged && Vector2.Angle(target.forward, transform.forward) < 10f && canDragged)
        {
            AutoComplete();
        }
    }

    void AutoComplete()
    {
        isAnyActive = false;
        activeFigure = null;
        moveMode = rotateMode = canDragged = false;
        transform.DOMove(target.position, 1f);
        transform.DORotate(target.eulerAngles, 1f);

        foreach (Renderer rend in gameObject.GetComponentsInChildren<Renderer>())
        {
            rend.material.SetColor("_EmissionColor", Color.black);
        }

        RotatePointer.Instance.Target = null;

        RenderManager.Instance.SetColor(Color.white, 0.0f);

        OnComplete();
    }

    public void SwitchRotateMode()
    {
        if (isSelected && canDragged)
        {
            if (isAnyActive && activeFigure != this)
                return;
            
            if (!rotateMode && moveMode)
            {
                Grab();
            }

            rotateMode = !rotateMode;

            rightAxis = VRCamera.Instance.transform.GetChild(0).right;
            upAxis = VRCamera.Instance.transform.GetChild(0).up;
            forwardAxis = VRCamera.Instance.transform.GetChild(0).forward;

            if (rotateMode)
            {
//                if (RenderManager.Instance)
//                    RenderManager.Instance.SetColor(Color.blue, 0.5f);
                isAnyActive = true;
                activeFigure = this;

                RotatePointer.Instance.Target = transform;

                foreach (Renderer rend in gameObject.GetComponentsInChildren<Renderer>())
                {
                    rend.material.SetColor("_EmissionColor", Color.cyan / 2f);
                }
            }
            else
            {
//                if (RenderManager.Instance)
//                    RenderManager.Instance.SetColor(Color.white, 0.0f);
                isAnyActive = false;
                activeFigure = null;

                RotatePointer.Instance.Target = null;
                    
                foreach (Renderer rend in gameObject.GetComponentsInChildren<Renderer>())
                {
                    rend.material.SetColor("_EmissionColor", Color.black);
                }
            }
        }
    }

    Vector3 upAxis, rightAxis, forwardAxis;

    void Grab()
    {
        if (isAnyActive && activeFigure != this)
            return;
        
        if (canDragged)
        {
            if (CheckFocus() || (!moveMode && rotateMode))
            {

                if (!moveMode && rotateMode)
                {
                    SwitchRotateMode();
                }

                if (moveMode = !moveMode)
                {
                    foreach (Renderer rend in gameObject.GetComponentsInChildren<Renderer>())
                    {
                        rend.material.SetColor("_EmissionColor", Color.yellow / 2f);
                    }

                    isAnyActive = true;
                    activeFigure = this;
                }
                else
                {
                    foreach (Renderer rend in gameObject.GetComponentsInChildren<Renderer>())
                    {
                        rend.material.SetColor("_EmissionColor", Color.black);
                    }

                    isAnyActive = false;
                    activeFigure = null;
                }
            }

//            if (transform.parent != VRCamera.Instance.transform.GetChild(0))
//            {
//                transform.parent = VRCamera.Instance.transform.GetChild(0);
//            }
//            else
//            {
//                transform.parent = null;
//            }
        }
    }

    void OnDrawGizmos() {
        if (Application.isPlaying)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(transform.position, VRCamera.Instance.transform.GetChild(0).right);

            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, VRCamera.Instance.transform.GetChild(0).up);

            Gizmos.color = Color.cyan;
            Gizmos.DrawRay(VRCamera.Instance.transform.position, transform.position - VRCamera.Instance.transform.position);
        }
    }

    Vector3 ax, ax1, ax2;

    void Rotate()
    {
        Vector3 cameraForward = VRCamera.Instance.transform.GetChild(0).forward;

        Vector3 viewTarget = VRCamera.Instance.transform.position + cameraForward * (Vector3.Distance(VRCamera.Instance.transform.position, transform.position));
        Vector3 axis = ax = Quaternion.Euler(0, 90, 0) * (VRCamera.Instance.transform.position - transform.position).normalized;
        //transform.RotateAround(transform.position, rightAxis, (viewTarget - transform.position).y * 5f);

        Vector3 rotateDirection = transform.position - viewTarget;

        ax1 = axis = Quaternion.Euler(90, 0, 0) * (VRCamera.Instance.transform.position - transform.position).normalized;
        rotateDirection.y = 0;

        Vector2 a = new Vector2(transform.position.x, transform.position.z);
        Vector2 b = new Vector2(viewTarget.x, viewTarget.z);

        float coef = 1;

        if (Vector2.Angle(-Vector2.right, a) < Vector2.Angle(Vector2.right, a))
            coef = Vector2.Angle(Vector2.up, a) > Vector2.Angle(Vector2.up, b) || Vector2.Angle(-Vector2.right, b) > Vector2.Angle(-Vector2.right, a) ? -1 : 1;
        else
            coef = Vector2.Angle(Vector2.up, a) > Vector2.Angle(Vector2.up, b) || (Vector2.Angle(-Vector2.right, b) < Vector2.Angle(-Vector2.right, a)) ? 1 : -1;



        if (Gestures.rotateMode == Gestures.RotateMode.Free || Gestures.rotateMode == Gestures.RotateMode.FreeAndZ)
        {
            transform.RotateAround(transform.position, rightAxis, (viewTarget - transform.position).y * 2.5f);
            transform.RotateAround(transform.position, upAxis, (Vector2.Angle(a, b) / 10f) * coef);

            if (Gestures.rotateMode == Gestures.RotateMode.FreeAndZ)
            {
                float angle = Gestures.Instance.transform.localEulerAngles.z;

                if (angle < 30)
                    angle *= -1;
                else if (angle > 270)
                    angle = 360 - angle;

                transform.RotateAround(transform.position, forwardAxis, -angle/7f);
            }

        }
        else if (Gestures.rotateMode == Gestures.RotateMode.Discrete2Axes || Gestures.rotateMode == Gestures.RotateMode.Discrete3Axes)
        {
            float x = Mathf.Abs((viewTarget - transform.position).y * 2.5f);
            float y = Mathf.Abs((Vector2.Angle(a, b) / 10f) * coef);

            if (x > 0.5f || y > 0.5f)
            {
                if (x > y)
                {
                    transform.RotateAround(transform.position, rightAxis, (viewTarget - transform.position).y * 2.5f);
                }
                else
                {
                    transform.RotateAround(transform.position, upAxis, (Vector2.Angle(a, b) / 10f) * coef);
                }
            }

            if (Gestures.rotateMode == Gestures.RotateMode.Discrete3Axes)
            {
                float angle = Gestures.Instance.transform.localEulerAngles.z;

                if (angle < 30)
                    angle *= -1;
                else if (angle > 270)
                    angle = 360 - angle;
                
                transform.RotateAround(transform.position, forwardAxis, -angle/7f);
            }
        }

    }

    void Update()
    {
        if (rotateMode)
        {
            Rotate();
        }

        if (moveMode)
        {
            Vector3 dir = VRCamera.Instance.transform.GetChild(0).forward;

            Vector3 viewTarget = VRCamera.Instance.transform.position + dir * activeDistance;
            transform.position = Vector3.Slerp(transform.position, viewTarget, Time.deltaTime * 8f);
        }
    }
}
