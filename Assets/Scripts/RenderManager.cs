﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderManager : MonoBehaviour 
{
    static RenderManager instance;

    static public RenderManager Instance
    {
        get
        {
            return instance;
        }
    }

    public NewLensCorrection[] normalLens;
    public NewLensCorrection[] outlineLens;
	// Use this for initialization
	void Start () {
        instance = this;
	}

    public void SetColor(Color color, float edgesOnly)
    {
//        if (edgesOnly > 0)
//            EnableOutline();
//        else
//            DisableOutline();

//        foreach (EdgeDetection cam in cameras)
//        {
//            cam.edgesOnlyBgColor = color;
//            cam.edgesOnly = edgesOnly;
//        }
    }

    public void EnableOutline()
    {

    }

    public void DisableOutline()
    {

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
