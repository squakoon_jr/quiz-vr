﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Gestures : MonoBehaviour 
{
    public enum RotateMode { Free, Discrete2Axes, Discrete3Axes, FreeAndZ };

    static public RotateMode rotateMode = RotateMode.Free;

    static public event Action TiltHeadRight;
    static public event Action TiltHeadLeft;

    bool isLeftTiltActive = false;
    bool isRightTiltActive = false;

    float actionAngle = 25;
    float freeAngle = 5;

    Figure selected;

    static Gestures instance;

    static public Gestures Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }

    void OnTiltHeadRight()
    {
        if (TiltHeadRight != null)
        {
            TiltHeadRight();
        }
    }

    void OnTiltHeadLeft()
    {
        if (TiltHeadLeft != null)
        {
            TiltHeadLeft();
        }
    }

    void Unselect()
    {
        if (selected && (!selected.moveMode && !selected.rotateMode))
        {
            selected.isSelected = false;

            RenderManager.Instance.DisableOutline();
            //selected.GetComponent<Renderer>().sharedMaterial.SetFloat("_Outline", 0f);
            selected = null;
            //RenderManager.Instance.DisableOutline();
        }
                
    }

    void UpdateOutline()
    {
        Vector3 dir = VRCamera.Instance.transform.GetChild(0).forward;

        RaycastHit hit;

        if (Physics.Raycast(VRCamera.Instance.transform.position, dir, out hit, 5))
        {
            if (hit.transform.GetComponent<Figure>())
            {
                
                Figure tempSelected = hit.transform.GetComponent<Figure>();

                if (selected && tempSelected != selected)
                {
                    if (selected.moveMode || selected.rotateMode)
                        return;
                    
                    Unselect();
                }

                selected = tempSelected;

                selected.isSelected = true;

                RenderManager.Instance.EnableOutline();
                //hit.transform.GetComponent<Renderer>().sharedMaterial.SetFloat("_Outline", 0.008f);
            }
            else
            {
                if (hit.transform.name.Equals("1"))
                {
                    Gestures.rotateMode = RotateMode.Free;
                    hit.transform.localScale = Vector3.one * 0.15f;
                    GameObject.Find("2").transform.localScale = Vector3.one * 0.1f;
                    GameObject.Find("3").transform.localScale = Vector3.one * 0.1f;
                    GameObject.Find("4").transform.localScale = Vector3.one * 0.1f;
                }
                else if (hit.transform.name.Equals("2"))
                {
                    Gestures.rotateMode = RotateMode.Discrete2Axes;
                    hit.transform.localScale = Vector3.one * 0.15f;
                    GameObject.Find("1").transform.localScale = Vector3.one * 0.1f;
                    GameObject.Find("3").transform.localScale = Vector3.one * 0.1f;
                    GameObject.Find("4").transform.localScale = Vector3.one * 0.1f;
                }
                else if (hit.transform.name.Equals("3"))
                {
                    Gestures.rotateMode = RotateMode.Discrete3Axes;
                    hit.transform.localScale = Vector3.one * 0.15f;
                    GameObject.Find("1").transform.localScale = Vector3.one * 0.1f;
                    GameObject.Find("2").transform.localScale = Vector3.one * 0.1f;
                    GameObject.Find("4").transform.localScale = Vector3.one * 0.1f;
                }
                else if (hit.transform.name.Equals("4"))
                {
                    Gestures.rotateMode = RotateMode.FreeAndZ;
                    hit.transform.localScale = Vector3.one * 0.15f;
                    GameObject.Find("1").transform.localScale = Vector3.one * 0.1f;
                    GameObject.Find("2").transform.localScale = Vector3.one * 0.1f;
                    GameObject.Find("3").transform.localScale = Vector3.one * 0.1f;
                }

                Unselect();
            }
        }
        else
        {
            Unselect();
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            OnTiltHeadRight();
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            OnTiltHeadLeft();
        }

        if (isLeftTiltActive)
        {
            if (transform.localEulerAngles.z < actionAngle - freeAngle)
            {
                isLeftTiltActive = false;
            }
        }
        else
        {
            if (transform.localEulerAngles.z > actionAngle && transform.localEulerAngles.z < 180 - 2*actionAngle)
            {
                isLeftTiltActive = true;
                OnTiltHeadLeft();
            }
        }


        if (isRightTiltActive)
        {
            if (transform.localEulerAngles.z > 360 - actionAngle + freeAngle)
            {
                isRightTiltActive = false;
            }
        }
        else
        {
            if (transform.localEulerAngles.z < 360 - actionAngle && transform.localEulerAngles.z > 360 - 2*actionAngle)
            {
                isRightTiltActive = true;
                OnTiltHeadRight();
            }
        }



        UpdateOutline();
    }

//    void OnGUI()
//    {
//        GUI.Box(new Rect(0, 0, 200, 50), transform.localEulerAngles.z.ToString());
//    }
	
}
