﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour 
{
    public event System.Action FinishLevelEvent;

    int completedFigures = 0;
    Figure[] figures;

    public string nextLevelScene;

    public float finishScale = 0.8f;

    void OnFinishLevel()
    {
        if (FinishLevelEvent != null)
        {
            FinishLevelEvent();
        }
    }

	
	void Start () 
    {
        figures = GameObject.FindObjectsOfType<Figure>();

        foreach (Figure f in figures)
        {
            f.CompleteEvent += CompleteFigure;
        }
	}

    void Finish()
    {
        foreach (Figure f in figures)
        {
            f.transform.SetParent(transform);
        }

        transform.DOScale(Vector3.one * finishScale, 2f);
        transform.DORotate(transform.eulerAngles + Vector3.up * 320f, 8f, RotateMode.FastBeyond360);

        Invoke("LoadNextLevel", 8f);
    }

    void LoadNextLevel()
    {
        if (!string.IsNullOrEmpty(nextLevelScene))
        {
            SceneManager.LoadScene(nextLevelScene);
            SceneManager.UnloadSceneAsync(0);
        }
    }

    void CompleteFigure()
    {
        ++completedFigures;

        if (completedFigures == figures.Length)
        {
            Invoke("Finish", 1.1f);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
